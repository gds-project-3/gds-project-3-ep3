﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ep5_ItemInEQ : MonoBehaviour {
    [SerializeField]
    private string itemName;
    [SerializeField]
    private string itemDesc;
    [SerializeField]
    private GameObject item;
    [SerializeField]
    private GameObject itemCombined;
    [SerializeField]
    private GameObject itemToCombine;
 
    public GameObject itemAfterUse;

    public string itemUsedText = "";

    public string itemCombinedText = "";

    ep5_Inventory eqController;

    Dictionary<int, KeyCode> keys;

    KeyCode[] keyss = new KeyCode[10];

    Vector3 positionInEQ;

    private int id;

    bool checkItem = false;

    private bool holdItem = false;

	// Use this for initialization
	void Start () {
        eqController = GetComponentInParent<ep5_Inventory>();
        MapKeys();
    }
	
	// Update is called once per frame
	void Update () {
        if (holdItem) {
            Vector3 mousePos = Input.mousePosition;
            Vector3 targetPos = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y,10f));
            transform.position = targetPos;
        }

        KeyUse();
    }

    //Methods
    private Collider2D[] PlayerInItemRange() {
        GameObject playerItemPicker = GameObject.Find("ItemPicker");
        CircleCollider2D circle2D = playerItemPicker.GetComponent<CircleCollider2D>();
        Vector3 circleBounds = circle2D.bounds.size;
        Vector3 circleCenter = circle2D.bounds.center;

        return Physics2D.OverlapBoxAll(circleCenter, circleBounds, 0f);
    }
    
    private Collider2D[] CheckCollidersBox() {
        BoxCollider2D box2d = GetComponent<BoxCollider2D>();
        Vector3 boxBounds = box2d.bounds.size;
        Vector3 boxCenter = box2d.bounds.center;

        return Physics2D.OverlapBoxAll(boxCenter, boxBounds, 0f);
    }
    
    private void ComboItem() { 
        Collider2D[] colliders = CheckCollidersBox();

        bool canCombine = false;

        foreach (Collider2D coll in colliders) {
            ep5_ItemInEQ comboCheck= coll.transform.GetComponent<ep5_ItemInEQ>(); ;

            if (comboCheck != null) {
                if (itemToCombine != null && coll.gameObject.name == string.Format("{0}(Clone)", itemToCombine.gameObject.name)) {
                    Destroy(coll.gameObject);
                    Destroy(gameObject);
                    eqController.EraseItem(id);
                    eqController.EraseItem(comboCheck.id);
                    eqController.AddItem(itemCombined);                    
                    GameObject.Find("Text").GetComponent<Text>().text = itemCombined.GetComponent<ep5_ItemInEQ>().itemCombinedText;
                    canCombine = true;
                    break;
                }                
            }

            if (!canCombine)
                GameObject.Find("Text").GetComponent<Text>().text = "tego nie da sie zkombinowac";
        }
    }

    public string ItemDesc() {
        return itemDesc;
    }

    public void HoldItem() {
        if (!holdItem) {
            holdItem = true;
            positionInEQ = transform.position;
        }
    }

    public void ReleaseItem() {
        //if (holdItem) {
            //holdItem = false;
            ComboItem();
            UseItem();
            //transform.position = positionInEQ;           
        //}
    }

    public void UseItem() {
        Collider2D[] colliders = PlayerInItemRange();
        bool canUse = false;

        foreach (Collider2D coll in colliders) {

            ep5_UsableThing objectOnUse = null;

            if (coll.transform.GetComponent<ep5_UsableThing>() != null)     
                objectOnUse = coll.transform.GetComponent<ep5_UsableThing>();
            
            if (objectOnUse!=null && gameObject.name == string.Format("{0}(Clone)", objectOnUse.ItemToTake.name)){               
                Destroy(coll.gameObject);
                Destroy(gameObject);
                eqController.EraseItem(id);
                GameObject.Find("Text").GetComponent<Text>().text = itemUsedText;
                canUse = true;

                if (itemAfterUse != null)
                    eqController.AddItem(itemAfterUse);

                break;               
            }
        }  

        if(!canUse)
            GameObject.Find("Text").GetComponent<Text>().text = "Nie moge tego tutaj uzyc";
    }

    public void DropItem() {
        eqController.DropItem(id);
        Destroy(gameObject);
    }

    public void SetID(int id) {
        this.id = id;
    }

    public GameObject GetReffernceItem() {
        return item;
    }

    void KeyUse() {
        KeyCode ky;

        keys.TryGetValue(id, out ky);

        if (Input.GetKeyDown(ky)) {
            UseItem();
        }
    }

    void MapKeys() {      
        keys = new Dictionary<int, KeyCode>();
        keys.Add(0, KeyCode.Alpha1);
        keys.Add(1, KeyCode.Alpha2);
        keys.Add(2, KeyCode.Alpha3);
        keys.Add(3, KeyCode.Alpha4);
        keys.Add(4, KeyCode.Alpha5);
        keys.Add(5, KeyCode.Alpha6);
        keys.Add(6, KeyCode.Alpha7);
        keys.Add(7, KeyCode.Alpha8);
        keys.Add(8, KeyCode.Alpha9);
    }
}
