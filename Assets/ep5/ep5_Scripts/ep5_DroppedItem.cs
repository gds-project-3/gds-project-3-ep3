﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ep5_DroppedItem : MonoBehaviour {
    ep5_Inventory eqController;
    [SerializeField]
    GameObject item;

    bool rightClick;

	// Use this for initialization
	void Start () {
        GameObject obj = GameObject.Find("ep5_Equipment");
        eqController = obj.GetComponent<ep5_Inventory>();
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void PickItem() {
        BoxCollider2D boxCollider = GetComponent<BoxCollider2D>();

        RaycastHit2D[] boxes=Physics2D.BoxCastAll(transform.position, boxCollider.bounds.size,0f,new Vector2(0f,0f));

        foreach(RaycastHit2D coll in boxes ) {
            if (coll.transform.gameObject.name == "Player") {
                if (eqController.AddItem(item)) {
                    GameObject.Find("Text").GetComponent<Text>().text = "Zebrano itema";
                    Destroy(gameObject);
                    return;
                }
                else {
                    GameObject.Find("Text").GetComponent<Text>().text = "Brak miejsca";
                    return;
                }
            }
        }

        GameObject.Find("Text").GetComponent<Text>().text = "Za daleko";

    }
}
