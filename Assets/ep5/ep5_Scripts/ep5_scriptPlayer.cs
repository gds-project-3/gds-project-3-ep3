﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ep5_scriptPlayer : MonoBehaviour {
    public float speed = 1.5f;
    private Vector3 target;
    int trafienie=0;
    // Use this for initialization
    void Start () {
        target = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Pressed left click, casting ray.");
            CastRay();
            target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            target.z = transform.position.z;
            
                transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
            
        }
        

        
    }
    void CastRay()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);
        if (hit)
        {
            Debug.Log(hit.collider.gameObject.name);
            trafienie = 1;
            
        }
    }
}
