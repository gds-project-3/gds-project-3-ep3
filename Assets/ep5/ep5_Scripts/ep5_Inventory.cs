﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ep5_Inventory : MonoBehaviour {
    [SerializeField]
    GameObject[] eq_list;
    
    GameObject emptySlot;

    float slotsSpace;

	// Use this for initialization
	void Start () {
        emptySlot = Resources.Load("ep5/ep5_EQEmptySlot", typeof(GameObject)) as GameObject;
        slotsSpace = emptySlot.GetComponent<SpriteRenderer>().bounds.center.x * 2;
        
        for (int i = 0; i < eq_list.Length; i++) {
            GameObject slotInst = Instantiate(emptySlot,transform);
            Vector3 vec = CalculatePosition(i);

            slotInst.transform.position = vec;

            if (eq_list[i] != null) {
                CreateIconItem(i,eq_list[i]);
            }
        }
    }

	// Update is called once per frame
	void Update () {
        

    }

   public void DropItem(int i) {
        Vector2 playerPos = GameObject.Find("Player").transform.position;
        GameObject inst = Instantiate(eq_list[i].GetComponent<ep5_ItemInEQ>().GetReffernceItem());
        inst.transform.position = new Vector3(Random.Range(playerPos.x-2, playerPos.x+2), Random.Range(playerPos.y-2, playerPos.y+2), 0f);
        eq_list[i]=null;
    }

    public void EraseItem(int i) {
        eq_list[i] = null;
    }

    public bool AddItem(GameObject item) {
        for (int i = 0; i < eq_list.Length; i++) {
            if (eq_list[i] == null) {
                eq_list[i] = item;
                CreateIconItem(i,item);
                return true;
            }
        }
        return false;
    }

    public void UseItem(GameObject item) {
    }

    Vector3 CalculatePosition(int pos) {
        Vector3 vec = new Vector3(pos * slotsSpace, 0f, 0f) + transform.position;
        return vec;
    }

    void CreateIconItem(int pos, GameObject itemObj) {
        Vector3 vec = CalculatePosition(pos);
        GameObject itemInst = Instantiate(itemObj, transform);

        itemInst.transform.position = vec;
        itemInst.GetComponent<ep5_ItemInEQ>().SetID(pos);
    }


}
