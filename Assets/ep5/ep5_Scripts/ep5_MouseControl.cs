﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ep5_MouseControl : MonoBehaviour {
    ep5_ItemInEQ itemInInventory;
    ep5_DroppedItem itemDropped;

	// Update is called once per frame
	void Update () {
        InteractItem();        
    }

    private void InteractItem() {
        bool rightClick = Input.GetMouseButtonDown(1);
        bool leftClick = Input.GetMouseButtonDown(0);

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D[] hits = Physics2D.RaycastAll(ray.origin, ray.direction, Mathf.Infinity);

        foreach (RaycastHit2D hit in hits) {
            if (hit) {

                itemInInventory = hit.transform.GetComponent<ep5_ItemInEQ>();
                itemDropped = hit.transform.GetComponent<ep5_DroppedItem>();

                if (itemInInventory != null) {
                    if (rightClick)
                        PopUp();

                    if (leftClick) {
                        itemInInventory.UseItem();
                        itemInInventory = null;
                    }
                }

                if (itemDropped != null) {
                    if (leftClick) {
                        itemDropped.PickItem();
                        itemDropped = null;
                    }
                }
            }
        }
    }

    private void PopUp() {
        GameObject.Find("Text").GetComponent<Text>().text = itemInInventory.ItemDesc();
    }
}
