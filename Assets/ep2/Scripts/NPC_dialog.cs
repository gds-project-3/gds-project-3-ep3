﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ep2.Scripts
{
	public class NPC_dialog : MonoBehaviour {

		private int skin = 0;

		private GameObject[] heads = new GameObject[15];
		private GameObject[] eyes = new GameObject[12];
		private GameObject[] mouth = new GameObject[12];
		private GameObject[] body = new GameObject[16];
		private GameObject[] handL = new GameObject[14];
		private GameObject[] handR = new GameObject[14];
		private Animator animator;

		// Use this for initialization
		void Start () {
			FindObjects ();
			animator = GetComponent<Animator> ();
		}
		
		// Update is called once per frame
		void Update () {
		}
			
		public int Skin {
			get { return skin;  }
			set { 
				skin = value; 
				SetSprites ();
			}
		}

		public List<int> SkinDetails {
			set {
				if (value.Count != 6)
					return;
				SetSprites (value [0], value [1], value [2], value [3], value [4], value [5]); 
			}
		}

		public void Show( bool isShow ) {
			animator.SetBool ("isShow", isShow);
		}

		public void Speaking( bool isSpeaking ) {
			animator.SetBool ("isSpeaking", isSpeaking);
		}

		private void SetSprites() {
			for (int i = 0; i < heads.Length; i++)
				heads [i].SetActive (i == skin || ( i==heads.Length-1 && skin>heads.Length-1) );

			for (int i = 0; i < eyes.Length; i++) {
				eyes [i].SetActive (i == skin || ( i==eyes.Length-1 && skin>eyes.Length-1));
			}

			for (int i = 0; i < mouth.Length; i++) {
				mouth [i].SetActive (i == skin || ( i==mouth.Length-1 && skin>mouth.Length-1));
			}

			for (int i = 0; i < body.Length; i++) {
				if( body[i] != null )
					body [i].SetActive (i == skin || ( i==body.Length-1 && skin>body.Length-1));
			}

			for (int i = 0; i < handR.Length; i++) {
				handR [i].SetActive (i == skin || ( i==handR.Length-1 && skin>handR.Length-1));
			}

			for (int i = 0; i < handL.Length; i++) {
				handL [i].SetActive (i == skin || ( i==handL.Length-1 && skin>handL.Length-1));
			}
		}

		private void SetSprites( int s_head, int s_eyes, int s_mouth, int s_body, int s_handR, int s_handL ) {
			for (int i = 0; i < heads.Length; i++)
				heads [i].SetActive (i == s_head );

			for (int i = 0; i < eyes.Length; i++) {
				eyes [i].SetActive (i == s_eyes );
			}

			for (int i = 0; i < mouth.Length; i++) {
				mouth [i].SetActive (i == s_mouth );
			}

			for (int i = 0; i < body.Length; i++) {
				if( body[i] != null ) 
					body [i].SetActive (i == s_body );
			}

			for (int i = 0; i < handR.Length; i++) {
				handR [i].SetActive (i == s_handR );
			}

			for (int i = 0; i < handL.Length; i++) {
				handL [i].SetActive (i == s_handL );
			}
		}

		private void FindObjects() {
		
			for (int i = 0; i < heads.Length; i++) {
				string name = string.Format ("ep4_NPC_head/ep4_NPC_head{0:D2}", i+1);
				heads [i] = this.transform.Find(name).gameObject;
			}

			for (int i = 0; i < eyes.Length; i++) {
				string name = string.Format ("ep4_NPC_eye/ep4_NPC_eye{0:D2}", i+1);
				eyes [i] = this.transform.Find(name).gameObject;
			}

			for (int i = 0; i < mouth.Length; i++) {
				string name = string.Format ("ep4_NPC_mouth/ep4_NPC_mouth{0:D2}", i+1);
				mouth [i] = this.transform.Find(name).gameObject;
			}

			for (int i = 0; i < body.Length; i++) {
				string name = string.Format ("ep4_NPC_body/ep4_NPC_body{0:D2}", i+1);
				if( this.transform.Find(name) )
					body [i] = this.transform.Find(name).gameObject;
			}

			for (int i = 0; i < handR.Length; i++) {
				string name = string.Format ("ep4_NPC_hand.R/ep4_NPC_hand.R{0:D2}", i+1);
				handR [i] = this.transform.Find(name).gameObject;
			}

			for (int i = 0; i < handL.Length; i++) {
				string name = string.Format ("ep4_NPC_hand.L/ep4_NPC_hand.L{0:D2}", i+1);
				handL [i] = this.transform.Find(name).gameObject;
			}
		
		}


	}
}
