﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ep2.Scripts
{
	
	public class CameraSortOdrer : MonoBehaviour {

		// Use this for initialization
		void Start () {
			
		}
		
		// Update is called once per frame
		void Update () {
			
		}

		private void LateUpdate()
		{
			var spriteRenderers = FindObjectsOfType<SpriteRenderer>();
			foreach (var sr in spriteRenderers)
			{
				if (sr.sortingLayerName == "Characters" || sr.sortingLayerName == "Decorations"  )
				{
					sr.sortingOrder = (int) Camera.main.WorldToScreenPoint(sr.bounds.min).y * -1;
				}
			}
		}
	}

}
