﻿using UnityEngine;

namespace ep3.Scripts
{
    public class Ep3Fallow : MonoBehaviour
    {
        public Transform ToFallow;

        private void LateUpdate()
        {
            transform.position = ToFallow.position + new Vector3(0, -1, 0);
        }
    }
}