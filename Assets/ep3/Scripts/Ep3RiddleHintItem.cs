﻿using Common.Scripts.Entity;
using Common.Scripts.Player;
using UnityEngine.UI;

namespace ep3.Scripts
{
    public class Ep3RiddleHintItem : Item
    {
        public Button HintButton;
        public Image Hint;

        private void Awake()
        {
            HintButton.gameObject.SetActive(false);
            Hint.gameObject.SetActive(false);
        }

        protected override bool OnInteract(Player player)
        {
            if (base.OnInteract(player))
            {
                HintButton.gameObject.SetActive(true);
            }
            return base.OnInteract(player);
        }
    }
}