﻿using Common.Scripts.Player;
using DG.Tweening;
using UnityEngine;

namespace ep3.Scripts
{
    public class Ep3SwitchMusicTrigger : MonoBehaviour
    {
        public AudioSource MusicPlayer;
        public AudioClip Clip;

        private Sequence _changeMusicSequence;
        private AudioClip _newClip;

        private void OnTriggerEnter2D(Collider2D other)
        {
            Debug.Log("Player detected on " + transform.name + ": switching music to: " + Clip.name);
            PlayMusic(Clip);
        }

        private void Awake()
        {
            PrepareChangeMusicSequence();
        }

        private void PlayMusic(AudioClip newClip)
        {
            if (MusicPlayer.clip == newClip || _changeMusicSequence.IsPlaying()) return;
            _newClip = newClip;
            _changeMusicSequence.Restart();
        }

        private void PrepareChangeMusicSequence()
        {
            _changeMusicSequence = DOTween.Sequence();
            _changeMusicSequence.Pause();
            _changeMusicSequence.SetAutoKill(false);
            var normalVolume = MusicPlayer.volume;
            var fadeMusic = MusicPlayer.DOFade(0.05f, 3f);
            fadeMusic.OnStart(() => { _newClip.LoadAudioData(); });
            fadeMusic.OnComplete(() =>
            {
                MusicPlayer.clip = _newClip;
                MusicPlayer.Play();
            });
            _changeMusicSequence.Append(fadeMusic);
            _changeMusicSequence.Append(MusicPlayer.DOFade(normalVolume, 2f));
        }

    }
}