﻿using Common.Scripts.Player;
using UnityEngine;
using UnityEngine.UI;

namespace ep3.Scripts
{
    public class Ep3DisableTrapTrigger : MonoBehaviour
    {

        public Ep3RiddleTrigger ToDisable;
        public Button HintButton;
        public GameObject ToEnable;

        private void OnTriggerEnter2D(Collider2D other)
        {
            var player = other.GetComponent<Player>();
            if (player == null) return;
            Debug.Log("Player detected on trap disabler, trap disabled!");
            ToDisable.ShouldKill = false;
            HintButton.gameObject.SetActive(false);
            ToEnable.gameObject.SetActive(true);
        }
    }
}