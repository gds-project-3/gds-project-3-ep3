﻿using Common.Scripts.Entity;
using Common.Scripts.Player;
using UnityEngine;

namespace ep3.Scripts
{
    public class Ep3TriggerPlayerPopup : MonoBehaviour
    {

        private Popup _popup;
        private Player _playerRef;
        private bool _triggered;

        private void Start()
        {
            _popup = GetComponent<Popup>();
            _playerRef = FindObjectOfType<Player>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (_triggered) return;
            Debug.Log("Player detected on " + transform.name + ": triggering popup: " + _popup.PopupNode);
            _popup.Interact(_playerRef);
            _triggered = true;
        }
    }
}