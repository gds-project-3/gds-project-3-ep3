﻿using Common.Scripts.Player;
using UnityEngine;

namespace ep3.Scripts
{
    public class Ep3RiddleTrigger : MonoBehaviour
    {
        public Transform PlayerRespawnLocation;
        public int DeathSoundIndex;
        public bool ShouldKill = true;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!ShouldKill) return;
            var player = other.GetComponent<Player>();
            if (player == null) return;
            Debug.Log("Player detected on riddle trap!!! " + transform.name + ": moving player to riddle start!");
            if (PlayerRespawnLocation.transform != null)
            {
                player.Stop();
                player.BlockPlayerInput();
                player.GetComponent<PlayerAudio>().PlayCustomSound(DeathSoundIndex);
                StartCoroutine(GetComponent<Ep3DeathFade>().FadeInAndOut(() => { player.transform.position = PlayerRespawnLocation.transform.position; },
                    player.AllowPlayerInput));
            }
        }
    }
}