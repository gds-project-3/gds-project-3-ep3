﻿using System;
using System.Collections;
using UnityEngine;

namespace ep3.Scripts
{
    public class Ep3DeathFade : MonoBehaviour
    {
        private Texture2D _blk;
        private float _alpha;

        public IEnumerator FadeInAndOut(Action onFadeInCallback, Action onFadeOutCallback)
        {
            _alpha = 0;
            while (_alpha < 1)
            {
                _alpha += Time.deltaTime * 5f;
                _blk.SetPixel(0, 0, new Color(0, 0, 0, _alpha));
                _blk.Apply();
                yield return null;
            }
            onFadeInCallback();
            while (_alpha > 0)
            {
                _blk.SetPixel(0, 0, new Color(0, 0, 0, _alpha));
                _blk.Apply();
                _alpha -= Time.deltaTime * .8f;
                yield return null;
            }

            onFadeOutCallback();
        }

        private void Start()
        {
            //make a tiny black texture
            _blk = new Texture2D(1, 1);
            _blk.SetPixel(0, 0, new Color(0, 0, 0, 0));
            _blk.Apply();
        }

        private void OnGUI()
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), _blk);
        }
    }
}