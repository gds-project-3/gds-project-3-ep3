﻿using UnityEngine;
using UnityEngine.UI;

namespace ep3.Scripts
{
    public class Ep3FlipHintActiveState : MonoBehaviour
    {
        public Image Hint;

        public void FlipActiveState()
        {
            Hint.gameObject.SetActive(!Hint.gameObject.activeInHierarchy);
        }
    }
}