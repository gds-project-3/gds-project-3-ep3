﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ep5_sceneTeleporter : MonoBehaviour {

    private void OnTriggerStay2D(Collider2D collision) {
        if (Input.GetKeyDown(KeyCode.Space)) {
            string goTo = collision.GetComponent<ep5_scene_teleporter>().teleportTo;
            string collName = collision.GetComponent<ep5_scene_teleporter>().teleporterName;

            ep5_scene_teleporter[] teleporters = FindObjectsOfType<ep5_scene_teleporter>();

            foreach(ep5_scene_teleporter teleporter in teleporters) {
                if (teleporter.teleporterName == goTo) {
                    transform.position = teleporter.transform.position;
                    GameObject.Find("CM vcam1").transform.position = teleporter.transform.position;
                }
            }
        }
    }
}
