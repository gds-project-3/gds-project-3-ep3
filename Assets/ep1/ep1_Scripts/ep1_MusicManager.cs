﻿using System.Collections;
using UnityEngine;

namespace ep1
{
    public class ep1_MusicManager : MonoBehaviour
    {
        private AudioSource audioSource;
        public float fadeTime = 0.5f;
        public AudioClip[] backgroundMusic = new AudioClip[0];
        public AudioClip questMusic;

        private int audioClipId = 0;

        private float startVolume;
        private bool playingQuestMusic = false;
        private float playTime = 0.0f;

        void Awake()
        {
            audioSource = GetComponent<AudioSource>();
            startVolume = audioSource.volume;
            audioClipId = Random.Range(0, 2);
            audioSource.clip = GetNextTrack();
            audioSource.Play();
        }

        void Update()
        {
            if (playingQuestMusic)
                return;

            playTime += Time.deltaTime;

            if (playTime > audioSource.clip.length)
            {
                audioSource.Stop();
                audioSource.clip = GetNextTrack();
                playTime = 0;
                audioSource.Play();
            }
        }

        private AudioClip GetNextTrack()
        {
            audioClipId++;

            if (audioClipId >= backgroundMusic.Length)
            {
                audioClipId = 0;
            }

            return backgroundMusic[audioClipId];
        }

        public void ChangeToQuestMusic()
        {
            StartCoroutine(Fade(0.0f));

            playingQuestMusic = true;
            audioSource.Stop();
            audioSource.clip = questMusic;
            audioSource.Play();

            StartCoroutine(Fade(startVolume));
        }

        public void ChangeToBackgroundMusic()
        {
            StartCoroutine(Fade(0.0f));

            audioSource.Stop();
            audioSource.clip = backgroundMusic[audioClipId];
            audioSource.time = playTime;
            audioSource.Play();
            playingQuestMusic = false;

            StartCoroutine(Fade(startVolume));
        }

        private IEnumerator Fade(float finalVolume)
        {
            float fadeSpeed = Mathf.Abs(audioSource.volume - finalVolume) / fadeTime;
            while (!Mathf.Approximately(audioSource.volume, finalVolume))
            {
                audioSource.volume = Mathf.MoveTowards(audioSource.volume, finalVolume, fadeSpeed * Time.deltaTime);
                yield return null;
            }
        }
    }
}
