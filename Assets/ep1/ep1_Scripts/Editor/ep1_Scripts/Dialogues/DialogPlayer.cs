﻿using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(DialogPlayer))]
    public class DialogPlayerEditor : Editor
    {
        private DialogPlayer dialogNpc;
        private SerializedProperty showOnSceneProperty;

        private const string dialogNpcPropShowOnSceneName = "showOnScene";

        private void OnEnable()
        {
            dialogNpc = (DialogPlayer)target;
            showOnSceneProperty = serializedObject.FindProperty(dialogNpcPropShowOnSceneName);
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            dialogNpc.ShowOnScene(EditorGUILayout.Toggle("Show on scene", showOnSceneProperty.boolValue));

            serializedObject.ApplyModifiedProperties();
        }
    }

}