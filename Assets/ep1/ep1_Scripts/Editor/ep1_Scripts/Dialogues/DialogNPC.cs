﻿using UnityEngine;
using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(DialogNPC))]
    public class DialogNPCEditor : Editor
    {
        private DialogNPC dialogNpc;
        private SerializedProperty npcVersionProperty;
        private SerializedProperty showOnSceneProperty;
        private SerializedProperty isSpeakingProperty;

        private const string dialogNpcPropNpcVersionName = "npcVersion";
        private const string dialogNpcPropShowOnSceneName = "showOnScene";
        private const string dialogNpcPropIsSpeakingName = "isSpeaking";

        private void OnEnable()
        {
            dialogNpc = (DialogNPC)target;

            npcVersionProperty = serializedObject.FindProperty(dialogNpcPropNpcVersionName);
            showOnSceneProperty = serializedObject.FindProperty(dialogNpcPropShowOnSceneName);
            isSpeakingProperty = serializedObject.FindProperty(dialogNpcPropIsSpeakingName);
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            dialogNpc.SetNpcVersion(EditorGUILayout.IntSlider("NPC Version", npcVersionProperty.intValue, 0, 10));
            dialogNpc.ShowOnScene(EditorGUILayout.Toggle("Show on scene", showOnSceneProperty.boolValue));
            dialogNpc.SetSpeaking(EditorGUILayout.Toggle("Is speaking", isSpeakingProperty.boolValue));

            serializedObject.ApplyModifiedProperties();
        }
    }
}
