﻿using UnityEngine;
using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(ep1_SwitchableSprite))]
    public class SwitchableSpriteEditor : Editor
    {
        private ep1_SwitchableSprite sprite;
        private GameObject scriptObject;
        private SpriteRenderer renderer;

        private void OnEnable()
        {
            sprite = (ep1_SwitchableSprite)target;
            scriptObject = sprite.gameObject;

            renderer = scriptObject.GetComponent<SpriteRenderer>();
        }

        public override void OnInspectorGUI()
        {
            if(!sprite.active)
            {
                renderer.sprite = sprite.inactiveSprite;
            }
            else
            {
                renderer.sprite = sprite.activeSprite;
            }

            DrawDefaultInspector();
        }
    }
}