﻿using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(ActivateGate))]
    public class ActivateGateEditor : ReactionEditor
    {
        protected override string GetFoldoutLabel()
        {
            return "Activate gate Reaction";
        }
    }
}
