﻿using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(ChangeStateOfInteractable))]
    public class ChangeStateOfInteractableEditor : ReactionEditor
    {
        protected override string GetFoldoutLabel()
        {
            return "Change interactable state Reaction";
        }
    }
}
