﻿using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(ChangeSceneState))]
    public class ChagneSceneStateEditor : ReactionEditor
    {
        protected override string GetFoldoutLabel()
        {
            return "ChagngeSceneState Reaction";
        }
    }
}
