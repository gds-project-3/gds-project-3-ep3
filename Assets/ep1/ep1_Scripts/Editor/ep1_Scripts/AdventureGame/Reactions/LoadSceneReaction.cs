﻿using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(LoadSceneReaction))]
    public class LoadSceneEditor : ReactionEditor
    {
        protected override string GetFoldoutLabel()
        {
            return "LoadSceneReaction";
        }
    }

}