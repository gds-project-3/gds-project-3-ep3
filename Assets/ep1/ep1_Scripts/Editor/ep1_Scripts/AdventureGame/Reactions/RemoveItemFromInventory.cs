﻿using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(RemoveItemFromInventory))]
    public class RemoveItemEditor : ReactionEditor
    {
        protected override string GetFoldoutLabel()
        {
            return "Remove Item Reaction";
        }
    }
}
