﻿using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(StartDialogueReaction))]
    public class StartDialogueReactionEditor : ReactionEditor
    {
        protected override string GetFoldoutLabel()
        {
            return "Start Dialogue Reaction";
        }
    }

}