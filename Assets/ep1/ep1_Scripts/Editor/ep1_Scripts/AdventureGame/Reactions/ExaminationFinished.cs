﻿using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(ExaminationFinished))]
    public class ExaminationFinishedEditor : ReactionEditor
    {
        protected override string GetFoldoutLabel()
        {
            return "Examinatin finished reaction";
        }
    }
}
