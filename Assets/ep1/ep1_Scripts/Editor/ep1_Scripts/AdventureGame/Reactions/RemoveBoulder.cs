﻿using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(RemoveBoulder))]
    public class RemoveBoulderEditor : ReactionEditor
    {
        protected override string GetFoldoutLabel()
        {
            return "Remove Boulder Reaction";
        }
    }
}