﻿using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(ChangeStateOfCollider))]
    public class ChangeStateOfColliderEditor : ReactionEditor
    {
        protected override string GetFoldoutLabel()
        {
            return "Change collider state Reaction";
        }
    }
}
