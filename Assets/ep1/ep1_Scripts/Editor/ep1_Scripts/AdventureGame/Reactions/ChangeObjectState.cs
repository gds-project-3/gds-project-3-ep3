﻿using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(ChangeObjectState))]
    public class ChangeObjectStateEditor : ReactionEditor
    {
        protected override string GetFoldoutLabel()
        {
            return "ChangeObjectState Reaction";
        }
    }
}
