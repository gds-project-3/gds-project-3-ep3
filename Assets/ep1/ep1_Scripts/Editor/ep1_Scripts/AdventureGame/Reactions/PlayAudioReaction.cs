﻿using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(PlayAudioReaction))]
    public class PlayAudioReactionEditor : ReactionEditor
    {
        protected override string GetFoldoutLabel()
        {
            return "Play Audio Reaction";
        }
    }

}
