﻿using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(RemoveObjectReaction))]
    public class RemoveObjectReactionEditor : ReactionEditor
    {
        protected override string GetFoldoutLabel()
        {
            return "Remove object Reaction";
        }
    }
}
