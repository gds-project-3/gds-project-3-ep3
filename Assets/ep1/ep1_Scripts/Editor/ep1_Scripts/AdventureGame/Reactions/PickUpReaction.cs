﻿using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(PickUpReaction))]
    public class PickedUpItemReactionEditor : ReactionEditor
    {
        protected override string GetFoldoutLabel()
        {
            return "Picked Up Item Reaction";
        }
    }
}
