﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(Conditions))]
    public class ConditionsEditor : EditorWithSubEditors<ConditionEditor, Condition>
    {
        public SerializedProperty conditionsCollectionsProperty;

        private Conditions conditions;

        private SerializedProperty conditionsProperty;
        private SerializedProperty reactionsProperty;

        private Type[] conditionTypes;
        private string[] conditionTypeNames;
        private int selectedIndex;

        private const float dropAreaHeight = 50f;
        private const float controlSpacing = 5f;
        private const string conditionsPropName = "conditions";
        private const string reactionsPropName = "reactions";

        private readonly float verticalSpacing = EditorGUIUtility.standardVerticalSpacing;

        private void OnEnable()
        {
            conditions = (Conditions)target;

            conditionsProperty = serializedObject.FindProperty(conditionsPropName);
            reactionsProperty = serializedObject.FindProperty(reactionsPropName);

            CheckAndCreateSubEditors(conditions.conditions);

            SetConditionNamesArray();
        }

        private void OnDisable()
        {
            CleanupEditors();
        }

        protected override void SubEditorSetup(ConditionEditor editor)
        {
            editor.conditionsProperty = conditionsProperty;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            CheckAndCreateSubEditors(conditions.conditions);

            for (int i = 0; i < subEditors.Length; i++)
            {
                subEditors[i].OnInspectorGUI();
            }

            if (conditions.conditions.Length > 0)
            {
                EditorGUILayout.Space();
                EditorGUILayout.Space();
            }

            Rect fullWidthRect = GUILayoutUtility.GetRect(GUIContent.none, GUIStyle.none, GUILayout.Height(dropAreaHeight + verticalSpacing));

            Rect leftAreaRect = fullWidthRect;
            leftAreaRect.y += verticalSpacing * 0.5f;
            leftAreaRect.width *= 0.5f;
            leftAreaRect.width -= controlSpacing * 0.5f;
            leftAreaRect.height = dropAreaHeight;

            Rect rightAreaRect = leftAreaRect;
            rightAreaRect.x += rightAreaRect.width + controlSpacing;

            TypeSelectionGUI(leftAreaRect);

            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(reactionsProperty);

            serializedObject.ApplyModifiedProperties();
        }

        private void TypeSelectionGUI(Rect containingRect)
        {
            Rect topHalf = containingRect;
            topHalf.height *= 0.5f;
            Rect bottomHalf = topHalf;
            bottomHalf.y += bottomHalf.height;

            selectedIndex = EditorGUI.Popup(topHalf, selectedIndex, conditionTypeNames);

            if (GUI.Button(bottomHalf, "Add Selected Condition"))
            {
                Type conditionType = conditionTypes[selectedIndex];
                Condition newCondition = ConditionEditor.CreateCondition(conditionType);
                conditionsProperty.AddToObjectArray(newCondition);
            }
        }

        public static Conditions CreateConditions()
        {
            Conditions newConditionCollection = CreateInstance<Conditions>();

            newConditionCollection.description = "New conditions collection";

            newConditionCollection.conditions = new Condition[0];

            return newConditionCollection;
        }

        private void SetConditionNamesArray()
        {
            Type reactionType = typeof(Condition);

            Type[] allTypes = reactionType.Assembly.GetTypes();

            List<Type> baseConditionSubTypeList = new List<Type>();

            for (int i = 0; i < allTypes.Length; i++)
            {
                if (allTypes[i].IsSubclassOf(reactionType) && !allTypes[i].IsAbstract)
                {
                    baseConditionSubTypeList.Add(allTypes[i]);
                }
            }

            conditionTypes = baseConditionSubTypeList.ToArray();

            List<string> reactionTypeNameList = new List<string>();

            for (int i = 0; i < conditionTypes.Length; i++)
            {
                reactionTypeNameList.Add(conditionTypes[i].Name);
            }

            conditionTypeNames = reactionTypeNameList.ToArray();
        }
    }
}
