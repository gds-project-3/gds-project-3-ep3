﻿using UnityEngine;
using UnityEditor;
using Common.Scripts.Player;

namespace ep1
{
    [CustomEditor(typeof(Interactable))]
    public class InteractableEditor : EditorWithSubEditors<ConditionsEditor, Conditions>
    {
        private Interactable interactable;

        private SerializedProperty interactionDistanceProperty;
        private SerializedProperty collectionsProperty;
        private SerializedProperty defaultReactionCollectionProperty;

        private const float collectionButtonWidth = 125f;
        private const string interactablePropInteractionDistanceName = "interactionDistance";
        private const string interactablePropConditionCollectionsName = "conditionsCollections";
        private const string interactablePropDefaultReactionCollectionName = "defaultReactions";

        private void OnEnable()
        {
            interactable = (Interactable)target;

            interactionDistanceProperty = serializedObject.FindProperty(interactablePropInteractionDistanceName);
            collectionsProperty = serializedObject.FindProperty(interactablePropConditionCollectionsName);
            defaultReactionCollectionProperty = serializedObject.FindProperty(interactablePropDefaultReactionCollectionName);

            CheckAndCreateSubEditors(interactable.conditionsCollections);
        }

        private void OnDisable()
        {
            CleanupEditors();
        }

        protected override void SubEditorSetup(ConditionsEditor editor)
        {
            editor.conditionsCollectionsProperty = collectionsProperty;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(interactionDistanceProperty);
            EditorGUILayout.Space();

            CheckAndCreateSubEditors(interactable.conditionsCollections);

            for (int i = 0; i < subEditors.Length; i++)
            {
                subEditors[i].OnInspectorGUI();
                EditorGUILayout.Space();
            }

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Add Collection", GUILayout.Width(collectionButtonWidth)))
            {
                Conditions newCollection = ConditionsEditor.CreateConditions();
                collectionsProperty.AddToObjectArray(newCollection);
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(defaultReactionCollectionProperty);
            serializedObject.ApplyModifiedProperties();
        }
    }


}