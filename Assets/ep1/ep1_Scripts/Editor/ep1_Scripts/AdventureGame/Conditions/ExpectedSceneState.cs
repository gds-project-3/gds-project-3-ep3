﻿using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(ExpectedSceneState))]
    public class ExpectedSceneStateEditor : ConditionEditor
    {
        protected override string GetFoldoutLabel()
        {
            return "Expected Scene State Condition";
        }
    }
}
