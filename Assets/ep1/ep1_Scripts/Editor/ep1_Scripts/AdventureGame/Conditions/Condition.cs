﻿using System;
using UnityEngine;
using UnityEditor;

namespace ep1
{
    public abstract class ConditionEditor : Editor
    {
        public bool showCondition;
        public SerializedProperty conditionsProperty;

        private Condition condition;
        private const float buttonWidth = 30f;

        private void OnEnable()
        {
            condition = (Condition)target;
            Init();
        }

        protected virtual void Init() { }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUI.indentLevel++;

            EditorGUILayout.BeginHorizontal();

            showCondition = EditorGUILayout.Foldout(showCondition, GetFoldoutLabel());

            if (GUILayout.Button("-", GUILayout.Width(buttonWidth)))
            {
                conditionsProperty.RemoveFromObjectArray(condition);
            }
            EditorGUILayout.EndHorizontal();

            if (showCondition)
            {
                DrawReaction();
            }

            EditorGUI.indentLevel--;
            EditorGUILayout.EndVertical();

            serializedObject.ApplyModifiedProperties();
        }

        public static Condition CreateCondition(Type conditionType)
        {
            return (Condition)CreateInstance(conditionType);
        }

        protected virtual void DrawReaction()
        {
            DrawDefaultInspector();
        }

        protected abstract string GetFoldoutLabel();
    }
}
