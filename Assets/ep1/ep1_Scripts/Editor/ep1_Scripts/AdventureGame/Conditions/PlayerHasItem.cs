﻿using UnityEditor;

namespace ep1
{
    [CustomEditor(typeof(PlayerHasItem))]
    public class PlayerHasItemEditor : ConditionEditor
    {
        protected override string GetFoldoutLabel()
        {
            return "Required Item Condition";
        }
    }
}
