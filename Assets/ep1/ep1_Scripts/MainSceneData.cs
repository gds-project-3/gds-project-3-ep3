﻿using System.Collections.Generic;
using UnityEngine;

namespace ep1
{
    [CreateAssetMenu(menuName= "EP1/MainSceneData")]
    public class MainSceneData : ScriptableObject
    {
        private void OnEnable()
        {
            var keys = new List<string>(stringBooleans.Keys);

            foreach(var key in keys)
            {
                stringBooleans[key] = false;
            }
        }

        public string playerSpawnPointName;
        
        [SerializeField]
        private StringBoolDictionary stringBoolStore = StringBoolDictionary.New<StringBoolDictionary>();
        public Dictionary<string, bool> stringBooleans
        {
            get { return stringBoolStore.dictionary; }
        }
    }
}
