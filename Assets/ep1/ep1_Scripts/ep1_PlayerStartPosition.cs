﻿using UnityEngine;

namespace ep1
{
    public class ep1_PlayerStartPosition : MonoBehaviour
    {
        public MainSceneData data;

        void Start()
        {
            var startingPoint = StartingPosition.FindPosition(data.playerSpawnPointName);
            if (startingPoint)
                transform.position = startingPoint.position;
            else
                Debug.Log("Can't find position " + data.playerSpawnPointName);

            if(data.stringBooleans["PlayerHasChangedClothes"])
            {
                 transform.Find("Meshes").gameObject.SetActive(true);
                 transform.Find("ep4_NPC_scena").gameObject.SetActive(false);
            }
        }
    }
}
