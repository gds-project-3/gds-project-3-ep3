﻿using UnityEngine;

namespace ep1
{
    public class ep1_PlayerCenter : MonoBehaviour
    {
        public Transform center;

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;

            Gizmos.matrix = Matrix4x4.TRS(center.position, Quaternion.identity, new Vector3(1, 1, 0));

            Gizmos.DrawWireSphere(Vector3.zero, 1.0f);
        }
    }
}