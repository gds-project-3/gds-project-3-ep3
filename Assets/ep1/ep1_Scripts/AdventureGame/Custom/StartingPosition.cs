﻿using System.Collections.Generic;
using UnityEngine;

namespace ep1
{
    public class StartingPosition : MonoBehaviour
    {
        public string positionName;

        private static Dictionary<string, StartingPosition> positions = new Dictionary<string, StartingPosition>();
        
        private void OnEnable()
        {
            positions[positionName] = this;
        }

        public static Transform FindPosition(string posName)
        {
            if (positions.ContainsKey(posName))
                return positions[posName].transform;
            else
                return null;
        }
    }
}
