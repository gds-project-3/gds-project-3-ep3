﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Common.Scripts.Player;

namespace ep1
{
    public class SceneController : MonoBehaviour
    {
        public CanvasGroup faderCanvasGroup;
        public float fadeDuration = 1f;
        public string startingSceneName = "ep1_map";
        public string initialStartingPositionName = "Tent";
        public MainSceneData saveData;

        private bool isFading;

        private IEnumerator Start()
        {
            faderCanvasGroup.alpha = 1f;

            yield return StartCoroutine(LoadSceneAndSetActive(startingSceneName, initialStartingPositionName));

            StartCoroutine(Fade(0f));
        }

        public void FadeAndLoadScene(LoadSceneReaction sceneReaction)
        {
            if (!isFading)
            {
                StartCoroutine(FadeAndSwitchScenes(sceneReaction));
            }
        }


        private IEnumerator FadeAndSwitchScenes(LoadSceneReaction sceneReaction)
        {
            yield return StartCoroutine(Fade(1f));

            yield return SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);
            if(sceneReaction.nextSceneName == "MainMenu")
            {
                yield return StartCoroutine(Fade(0f));
                yield return SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);
                yield return SceneManager.LoadSceneAsync("MainMenu", LoadSceneMode.Single);

                Scene newlyLoadedScene = SceneManager.GetSceneAt(SceneManager.sceneCount - 1);
                SceneManager.SetActiveScene(newlyLoadedScene);
            }
            else
            {
                yield return StartCoroutine(LoadSceneAndSetActive(sceneReaction.nextSceneName, sceneReaction.nextLocationName));

                if(saveData.stringBooleans.ContainsKey("CraneHasBeenFixed") && saveData.stringBooleans["CraneHasBeenFixed"])
                    GameObject.Find("ep1_body").SetActive(true);

                yield return StartCoroutine(Fade(0f));
            }
            
        }


        private IEnumerator LoadSceneAndSetActive(string sceneName, string nextLocationName)
        {
            yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

            Scene newlyLoadedScene = SceneManager.GetSceneAt(SceneManager.sceneCount - 1);

            Player player = FindObjectOfType<Player>();

            if (player) //not every scene has player
                player.transform.position = StartingPosition.FindPosition(nextLocationName).position;

            SceneManager.SetActiveScene(newlyLoadedScene);
        }

        private IEnumerator Fade(float finalAlpha)
        {
            isFading = true;

            faderCanvasGroup.blocksRaycasts = true;

            float fadeSpeed = Mathf.Abs(faderCanvasGroup.alpha - finalAlpha) / fadeDuration;

            while (!Mathf.Approximately(faderCanvasGroup.alpha, finalAlpha))
            {
                faderCanvasGroup.alpha = Mathf.MoveTowards(faderCanvasGroup.alpha, finalAlpha, fadeSpeed * Time.deltaTime);
                yield return null;
            }

            isFading = false;

            faderCanvasGroup.blocksRaycasts = false;
        }
    }
}
