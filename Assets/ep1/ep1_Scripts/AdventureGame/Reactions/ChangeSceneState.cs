﻿namespace ep1
{
    public class ChangeSceneState : Reaction
    {
        public MainSceneData data;
        public string key;
        public bool value;

        public override void React()
        {
            data.stringBooleans[key] = value;
        }
    }
}
