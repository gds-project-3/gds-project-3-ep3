﻿namespace ep1
{
    public class RemoveItemFromInventory : Reaction
    {
        public InventoryItem item;
        public int quantity = 1;

        public override void React()
        {
            Inventory inventory = FindObjectOfType<Inventory>();
            for (int i = 0; i < quantity; i++)
            {
                inventory.RemoveItem(item);
            }
        }
    }
}
