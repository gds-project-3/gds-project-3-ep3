﻿using UnityEngine;

namespace ep1
{
    public class TextReaction : Reaction
    {
        public string message;
        public Color textColor = Color.white;
        public float delay;

        private TextManager textManager;

        private void OnEnable()
        {
            textManager = FindObjectOfType<TextManager>();
        }

        public override void React()
        {
            textManager.DisplayMessage(message, textColor, delay);
        }
    }
}
