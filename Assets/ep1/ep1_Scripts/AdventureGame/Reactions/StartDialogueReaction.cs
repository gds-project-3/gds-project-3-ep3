﻿namespace ep1
{
    public class StartDialogueReaction : Reaction
    {
        public Dialogue[] dialogues = new Dialogue[1];
        public float delay = 0.0f;

        public override void React()
        {
            foreach (var dialogue in dialogues)
            {
                FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
            }
        }
    }
}
