﻿using UnityEngine;

namespace ep1
{
    public class ExaminationFinished : Reaction
    {
        public MainSceneData sceneData;

        public override void React()
        {
            //FIXME: remove hardcode
            if (sceneData.stringBooleans["BottleClicked2"] &&
                sceneData.stringBooleans["BloodClicked2"] &&
                sceneData.stringBooleans["PocketClicked2"] &&
                sceneData.stringBooleans["WellClicked2"] &&
                sceneData.stringBooleans["BodyClicked2"])
            {
                FindObjectOfType<TextManager>().DisplayMessage("Obdukcja skończona, czas ruszać...", Color.white, 0.0f);
                TriggerSceneChange();
            }
        }

        void TriggerSceneChange()
        {
            LoadSceneReaction reaction = ScriptableObject.CreateInstance("LoadSceneReaction") as LoadSceneReaction;
            reaction.nextSceneName = "ep1_map";
            reaction.nextLocationName = "Crane";
            reaction.React();
        }
    }
}
