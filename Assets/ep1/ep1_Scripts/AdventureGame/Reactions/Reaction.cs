﻿using UnityEngine;

namespace ep1
{
    public abstract class Reaction : ScriptableObject
    {
        public virtual void React()
        {
        }
    }
}
