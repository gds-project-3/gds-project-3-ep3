﻿namespace ep1
{
    public class ChangeStateOfInteractable : Reaction
    {
        public Interactable interactable;
        public bool enabled = false;

        public override void React()
        {
            interactable.DisableInteractions(enabled);
        }
    }
}
