﻿using UnityEngine;

namespace ep1
{
    public class PlayAudioReaction : Reaction
    {
        public AudioSource audioSource;
        public AudioClip audioClip;
        public float delay;

        public override void React()
        {
            audioSource.clip = audioClip;
            audioSource.Play();
        }
    }
}
