﻿using UnityEngine;

namespace ep1
{
    public class Reactions : MonoBehaviour
    {
        public Reaction[] reactions = new Reaction[0];

        public void React()
        {
            foreach (var reaction in reactions)
            {
                reaction.React();
            }
        }
    }
}
