﻿using UnityEngine;

namespace ep1
{
    public class ChangeStateOfCollider : Reaction
    {
        public Collider2D collider;
        public bool enabled = false;

        public override void React()
        {
            collider.enabled = enabled;
        }
    }
}
