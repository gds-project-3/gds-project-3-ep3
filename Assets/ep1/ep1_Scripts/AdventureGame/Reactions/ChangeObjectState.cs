﻿using UnityEngine;

namespace ep1
{
    public class ChangeObjectState : Reaction
    {
        public GameObject go;
        public bool enabled;

        public override void React()
        {
            go.SetActive(enabled);
        }
    }
}
