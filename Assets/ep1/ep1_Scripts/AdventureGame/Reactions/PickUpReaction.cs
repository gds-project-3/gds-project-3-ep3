﻿namespace ep1
{
    public class PickUpReaction : Reaction
    {
        public InventoryItem item;

        private Inventory inventory;

        public void OnEnable()
        {
            inventory = FindObjectOfType<Inventory>();
        }

        public override void React()
        {
            inventory.AddItem(item);
        }
    }
}
