﻿using UnityEngine;

namespace ep1
{
    public class LoadSceneReaction : Reaction
    {
        public string nextSceneName;
        public string nextLocationName;
        private SceneController sceneController;

        private void OnEnable()
        {
            sceneController = FindObjectOfType<SceneController>();
        }

        public override void React()
        {
            if (sceneController == null)
            {
#if UNITY_EDITOR
                // Application.Quit() does not work in the editor so
                // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
                UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
            }
            else
            {
                sceneController.FadeAndLoadScene(this);
            }
        }
    }
}
