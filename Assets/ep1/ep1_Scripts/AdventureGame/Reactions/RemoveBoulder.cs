﻿using UnityEngine;

namespace ep1
{
    public class RemoveBoulder : Reaction
    {
        public Transform newLocation;
        public Transform boulder;

        public override void React()
        {
            boulder.position = newLocation.position;
            ep1_SwitchableSprite[] switchables = FindObjectsOfType<ep1_SwitchableSprite>();
            for (int i = 0; i < switchables.Length; i++)
            {
                if (switchables[i].type == ep1_SwitchableSprite.AssetType.River)
                {
                    switchables[i].Activate();
                }
            }
        }
    }
}
