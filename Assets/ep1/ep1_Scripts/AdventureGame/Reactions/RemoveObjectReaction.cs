﻿using UnityEngine;

namespace ep1
{
    public class RemoveObjectReaction : Reaction
    {
        public GameObject obj;

        public override void React()
        {
            Destroy(obj);
        }
    }
}
