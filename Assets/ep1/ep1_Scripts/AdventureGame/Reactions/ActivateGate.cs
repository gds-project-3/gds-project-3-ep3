﻿using UnityEngine;

namespace ep1
{
    public class ActivateGate : Reaction
    {
        public override void React()
        {
            ep1_SwitchableSprite[] switchables = FindObjectsOfType<ep1_SwitchableSprite>();
            for (int i = 0; i < switchables.Length; i++)
            {
                if (switchables[i].type == ep1_SwitchableSprite.AssetType.Gate)
                {
                    switchables[i].Activate();
                    switchables[i].gameObject.GetComponent<BoxCollider2D>().enabled = false;
                }
            }
        }
    }
}
