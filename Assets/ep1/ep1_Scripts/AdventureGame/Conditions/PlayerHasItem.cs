﻿namespace ep1
{
    public class PlayerHasItem : Condition
    {
        public InventoryItem item;
        public int quantitiy = 1;

        public override bool IsSatisfied()
        {
            Inventory inventory = FindObjectOfType<Inventory>();

            return inventory.HasItem(item, quantitiy);
        }
    }
}
