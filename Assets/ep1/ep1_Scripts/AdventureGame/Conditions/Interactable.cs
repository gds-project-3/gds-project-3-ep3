﻿using UnityEngine;
using Common.Scripts.Player;

namespace ep1
{
    //[RequireComponent(typeof(Collider2D))]
    public class Interactable : MonoBehaviour
    {
        public float interactionDistance = 1.0f;
        public Conditions[] conditionsCollections = new Conditions[0];
        public Reactions defaultReactions;

        private Player player;
        private Transform playerCenter;

        private SpriteGlow.SpriteGlowEffect glow;

        public bool isEnabled = true;

        public void DisableInteractions(bool v)
        {
            isEnabled = v;
            if (glow)
                glow.enabled = false;
        }

        private void Start()
        {
            if (!isEnabled)
            {
                return;
            }

            player = FindObjectOfType<Player>();
            if(player)
                playerCenter = player.GetComponent<ep1_PlayerCenter>().center;
            glow = GetComponent<SpriteGlow.SpriteGlowEffect>();
            if(glow)
                glow.enabled = false;
        }

        private void OnMouseDown()
        {
            if (!isEnabled)
            {
                return;
            }
            Interact();
        }

        private void OnMouseEnter()
        {
            if (!isEnabled)
            {
                return;
            }
            if (glow)
                glow.enabled = true;
        }

        private void OnMouseExit()
        {
            if (!isEnabled)
            {
                return;
            }
            if (glow)
                glow.enabled = false;
        }

        public void Interact()
        {
            if (!isEnabled)
            {
                return;
            }
            if (IsInInteractionDistance())
            {
                for (int i = 0; i < conditionsCollections.Length; i++)
                {
                    if (conditionsCollections[i].CheckAndReact())
                        return;
                }
            }
            defaultReactions.React();
        }

        private bool IsInInteractionDistance()
        {
            return interactionDistance < 0 || Vector2.Distance(playerCenter.position, transform.position) < interactionDistance;
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;

            Gizmos.matrix = Matrix4x4.TRS(transform.position, Quaternion.identity, new Vector3(1, 1, 0));

            Gizmos.DrawWireSphere(Vector3.zero, interactionDistance);
        }
    }
}
