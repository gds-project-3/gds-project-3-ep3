﻿using UnityEngine;

namespace ep1
{
    public class Conditions : ScriptableObject
    {
        public string description;
        public Condition[] conditions = new Condition[0];
        public Reactions reactions;

        public bool CheckAndReact()
        {
            foreach (var condition in conditions)
                if (!condition.IsSatisfied())
                    return false;

            if (reactions)
                reactions.React();

            return true;
        }
    }
}
