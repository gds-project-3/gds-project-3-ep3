﻿namespace ep1
{
    public class ExpectedSceneState : Condition
    {
        public MainSceneData data;
        public string key;
        public bool value;

        public override bool IsSatisfied()
        {
            return data.stringBooleans[key] == value;
        }
    }
}
