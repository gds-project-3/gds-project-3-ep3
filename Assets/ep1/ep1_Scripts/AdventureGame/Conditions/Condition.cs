﻿using UnityEngine;

namespace ep1
{
    public abstract class Condition : ScriptableObject
    {
        public abstract bool IsSatisfied();
    }
}
