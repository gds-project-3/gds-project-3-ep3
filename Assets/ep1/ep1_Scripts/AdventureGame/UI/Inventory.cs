﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace ep1
{
    public class Inventory : MonoBehaviour
    {
        public const int numItemSlots = 4;
        public Image[] itemImages = new Image[4];

        public InventoryItem[] items = new InventoryItem[numItemSlots];

        public void AddItem(InventoryItem itemToAdd)
        {
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] == null)
                {
                    items[i] = itemToAdd;
                    itemImages[i].sprite = itemToAdd.sprite;
                    itemImages[i].enabled = true;
                    return;
                }
            }
        }

        public void RemoveItem(InventoryItem itemToRemove)
        {
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] == itemToRemove)
                {
                    items[i] = null;
                    itemImages[i].sprite = null;
                    itemImages[i].enabled = false;
                    return;
                }
            }
        }

        public bool HasItem(InventoryItem item, int quantity)
        {
            return items.Count(i => i == item) >= quantity;
        }
    }
}
