﻿using UnityEngine;

namespace ep1
{
    [CreateAssetMenu(menuName = "EP1/InventoryItem")]
    public class InventoryItem : ScriptableObject
    {
        public Sprite sprite;
    }
}
