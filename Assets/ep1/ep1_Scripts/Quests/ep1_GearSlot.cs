﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ep1_GearSlot : MonoBehaviour, IDropHandler
{
    public int slotNumber = 0;
    private ep1_Mechanism listener;

    public GameObject item
    {
        get
        {
            if (transform.childCount > 0)
            {
                return transform.GetChild(0).gameObject;
            }
            return null;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (!item)
        {
            ep1_DragHandler.itemBeingDragged.transform.SetParent(transform);
            Notify(true);
        }
    }

    public void Register(ep1_Mechanism client)
    {
        listener = client;
    }

    public void Notify(bool hasGear)
    {
        if (listener)
        {
            listener.StateChanged(this, hasGear);
        }
    }
}
