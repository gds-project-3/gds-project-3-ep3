﻿using UnityEngine;

namespace ep1
{
    public class ep1_SwitchableSprite : MonoBehaviour
    {
        public enum AssetType { River, Gate, Crane}

        public AssetType type;

        public bool active = false;

        public Sprite activeSprite;
        public Sprite inactiveSprite;

        private SpriteRenderer spriteRenderer;

        private void Start()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            if (active)
            {
                spriteRenderer.sprite = activeSprite;
            }
            else
            {
                spriteRenderer.sprite = inactiveSprite;
            }
        }

        public void Activate()
        {
            active = true;
            spriteRenderer.sprite = activeSprite;
        }
    }
}
