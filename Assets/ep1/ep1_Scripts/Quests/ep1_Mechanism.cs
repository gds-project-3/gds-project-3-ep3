﻿using UnityEngine;
using ep1;

public class ep1_Mechanism : MonoBehaviour
{
    public int spinSpeed = 10;

    public ep1_GearSlot[] slots;
    public ep1_Gear gear1;
    public ep1_Chain chain;
    public MainSceneData data;

    private void Start()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].Register(this);
        }
    }

    public void StateChanged(ep1_GearSlot slot, bool hasGear)
    {
        slot.GetComponent<ep1_Gear>().rotating = hasGear;
    }

    private void Update()
    {
        gear1.Rotate(spinSpeed);

        if (chain.IsAtFinalPosition())
        {
            data.stringBooleans["CraneHasBeenFixed"] = true;
        }

        if (Input.GetKeyDown(KeyCode.Escape) || chain.IsAtFinalPosition())
        {
            LoadSceneReaction reaction = ScriptableObject.CreateInstance("LoadSceneReaction") as LoadSceneReaction;
            reaction.nextSceneName = "ep1_map";
            reaction.nextLocationName = "Crane";
            reaction.React();
        }
    }
}
