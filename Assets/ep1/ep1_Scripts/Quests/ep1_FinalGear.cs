﻿using UnityEngine;

public class ep1_FinalGear : ep1_Gear
{
    public ep1_Gear invalidGear;
    public ep1_Chain chain;

    public override bool TryRotate()
    {
        if (invalidGear.transform.childCount > 0)
        {
            return chain.TryPullUp();
        }
        else
        {
            return chain.TryPullDown();
        }
    }

    public override bool Rotate(int speed)
    {
        if (!TryRotate())
            return false;

        if (invalidGear.transform.childCount > 0)
        {
            if (!chain.PullUp())
            {
                return false;
            }

            for (int i = 0; i < dependentGears.Length; i++)
            {
                ep1_Gear gear = dependentGears[i];
                if (!gear.rotating)
                {
                    break;
                }

                Vector3 direction = gear.direction == Direction.Clockwise ? Vector3.forward : spinDirection = Vector3.back;

                dependentGears[i].transform.Rotate(direction * speed * Time.deltaTime);
            }

            transform.Rotate(-spinDirection * speed * Time.deltaTime);

            return true;
        }
        else
        {
            if(!chain.PullDown())
            {
                return false;
            }

            transform.Rotate(spinDirection * speed * Time.deltaTime);

            return true;
        }
    }
}
