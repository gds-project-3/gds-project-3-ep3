﻿using UnityEngine;

namespace ep1
{
    public class ep1_CallInteractableOnTriggerEnter : MonoBehaviour
    {
        private Interactable interactable;
        void Start()
        {
            interactable = GetComponent<Interactable>();
        }

        void OnTriggerEnter2D(Collider2D collider)
        {
            interactable.Interact();
        }
    }
}

