﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ep1_DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public static GameObject itemBeingDragged;

    private Transform startParent;
    private Vector3 startPosition;

    public void OnBeginDrag(PointerEventData eventData)
    {
        startParent = transform.parent;
        itemBeingDragged = gameObject;
        startPosition = transform.position;
        GetComponent<CanvasGroup>().blocksRaycasts = false;

        NotifyParent(false);
    }

    public void OnDrag(PointerEventData eventData)
    {
        var pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pos.z = 0f;

        transform.position = pos;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        itemBeingDragged = null;

        GetComponent<CanvasGroup>().blocksRaycasts = true;
        if(startParent == transform.parent)
        {
            transform.position = startPosition;
            NotifyParent(true);
        }
    }

    private void NotifyParent(bool hasGear)
    {
        ep1_GearSlot[] parentSlot = GetComponentsInParent<ep1_GearSlot>();

        if (parentSlot != null)
        {
            parentSlot[0].Notify(hasGear);
        }
    }
}
