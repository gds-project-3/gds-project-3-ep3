﻿using UnityEngine;

public class ep1_ComplexGear : ep1_Gear
{
    private bool blocked = false;

    private Quaternion finalRotation;

    private void Start()
    {
        finalRotation = Quaternion.Euler(new Vector3(0, 0, 0));
    }

    public override bool TryRotate()
    {
        return !blocked;
    }

    public override bool Rotate(int speed)
    {
        if (blocked)
            return false;

        transform.rotation = Quaternion.RotateTowards(transform.rotation, finalRotation, speed * Time.deltaTime * (direction == Direction.Clockwise ? -1 : 1));

        if(transform.rotation == finalRotation)
        {
            foreach (Transform child in transform)
            {
                child.gameObject.GetComponent<ep1_Gear>().rotating = true;
            }

            blocked = true;
        }

        return !blocked;
    }
}
