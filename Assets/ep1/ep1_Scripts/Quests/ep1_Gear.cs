﻿using UnityEngine;

public class ep1_Gear : MonoBehaviour
{
    public enum Direction { Clockwise, Counterclockwise };
    public Direction direction;

    public bool rotating = false;
    public ep1_Gear[] dependentGears = new ep1_Gear[1];

    protected Vector3 spinDirection;

    private void Start()
    {
        if (direction == Direction.Clockwise)
        {
            spinDirection = Vector3.back;
        }
        else
        {
            spinDirection = Vector3.forward;
        }
    }

    public virtual bool TryRotate()
    {
        if (rotating)
        {
            for (int i = 0; i < dependentGears.Length; i++)
            {
                if (!dependentGears[i].TryRotate())
                    return false;
            }
        }

        return true;
    }

    public virtual bool Rotate(int speed)
    {
        if (!TryRotate())
        {
            return false;
        }

        if (rotating)
        {
            for (int i = 0; i < dependentGears.Length; i++)
            {
                if(!dependentGears[i].Rotate(speed))
                    return false;
            }

            transform.Rotate(spinDirection * speed * Time.deltaTime);
        }

        return true;
    }
}
