﻿using UnityEngine;

public class ep1_Chain : MonoBehaviour
{
    public float finalY;
    public float minimalY;
    public float chainSpeed;

    Vector3 minimalPosition;
    Vector3 finalPosition;

    private void Start()
    {
        minimalPosition = new Vector3(transform.position.x, minimalY, transform.position.z);
        finalPosition = new Vector3(transform.position.x, finalY, transform.position.z);
    }

    public bool PullUp()
    {
        transform.position = Vector3.MoveTowards(transform.position, minimalPosition, Time.deltaTime * chainSpeed);

        return transform.position.y != minimalPosition.y;
    }

    public bool PullDown()
    {
        transform.position = Vector3.MoveTowards(transform.position, finalPosition, Time.deltaTime * chainSpeed);

        return transform.position.y != finalPosition.y;
    }

    public bool TryPullUp()
    {
        return transform.position.y != minimalPosition.y;
    }

    public bool TryPullDown()
    {
        return transform.position.y != finalPosition.y;
    }

    public bool IsAtFinalPosition()
    {
        return Mathf.Approximately(transform.position.y, finalPosition.y);
    }
}
