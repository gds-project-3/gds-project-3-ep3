﻿using UnityEngine;
using SpriteGlow;


[RequireComponent(typeof(SpriteGlowEffect))]
public class DisableGlowOnStart : MonoBehaviour
{
	void Start ()
    {
        GetComponent<SpriteGlowEffect>().enabled = false;
    }
}
