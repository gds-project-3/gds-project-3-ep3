﻿using UnityEngine;

namespace ep1
{

    public class DialogPlayer : MonoBehaviour
    {
        [SerializeField]
        private bool showOnScene = false;

        private Animator animator;

        private void Start()
        {
            animator = GetComponent<Animator>();
            ShowOnScene(showOnScene);
        }

        public void ShowOnScene(bool isShow)
        {
            showOnScene = isShow;
            if (animator != null)
                animator.SetBool("show_dialog", isShow);
        }
    }
}
