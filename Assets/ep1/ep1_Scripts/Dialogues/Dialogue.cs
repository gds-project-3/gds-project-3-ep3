﻿using System;
using UnityEngine;

namespace ep1
{
    [Serializable]
    public class Sentence
    {
        [TextArea(3, 10)]
        public string[] texts = new string[1];
        public string speakingPersonName;
        [Range(1, 10)]
        public int npcId = 2;
        public bool player = false;
    }

    [CreateAssetMenu(menuName ="EP1/Dialogue")]
    public class Dialogue : ScriptableObject
    {
        public Sentence[] sentences = new Sentence[1];
    }
}
