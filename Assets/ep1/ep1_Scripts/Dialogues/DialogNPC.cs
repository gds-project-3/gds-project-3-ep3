﻿using UnityEngine;

namespace ep1
{
    public class DialogNPC : MonoBehaviour
    {
        [SerializeField]
        private int npcVersion = 0;
        [SerializeField]
        private bool showOnScene = false;
        [SerializeField]
        private bool isSpeaking = false;

        private Animator animator;

        private void Start()
        {
            animator = GetComponent<Animator>();
            ShowOnScene(showOnScene);
            SetSpeaking(isSpeaking);
        }

        public void ShowOnScene(bool isShow)
        {
            showOnScene = isShow;
            if (animator != null)
                animator.SetBool("isShow", isShow);
        }

        public void SetSpeaking(bool isSpeaking)
        {
            this.isSpeaking = isSpeaking;
            if (animator != null)
                animator.SetBool("isSpeaking", isSpeaking);
        }

        public void SetNpcVersion(int npcVersion)
        {
            this.npcVersion = npcVersion;
            for (int i = 0; i < transform.childCount - 1; i++)
            {
                var child = transform.GetChild(i);
                foreach(Transform bodyPart in child)
                {
                    bodyPart.gameObject.SetActive(false);
                }
                child.GetChild(npcVersion).gameObject.SetActive(true);
            }
        }
    }
}
