﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Common.Scripts.Player;

namespace ep1
{
    public class DialogueManager : MonoBehaviour
    {
        private class DialogueData
        {
            public int npcId = -1;
            public bool playerSpeaking = true;
            public string sentence;
        }

        public Physics2DRaycaster raycaster;

        public DialogNPC npcDialog;
        public DialogPlayer playerDialog;
        
        public Text nameText;
        public Text dialogueText;

        public Animator animator;

        private Player player;
        private Queue<DialogueData> sentences;

        void Start()
        {
            player = (Player)FindObjectOfType(typeof(Player));
            sentences = new Queue<DialogueData>();
        }

        public void StartDialogue(Dialogue dialogue)
        {
            if(dialogue.sentences.Length > 1)
                npcDialog.ShowOnScene(true);
            playerDialog.ShowOnScene(true);
            animator.SetBool("IsOpen", true);

            raycaster.enabled = false;
            player.BlockPlayerInput();

            nameText.text = dialogue.name;

            foreach (Sentence sentence in dialogue.sentences)
            {
                foreach (var text in sentence.texts)
                {
                    DialogueData data = new DialogueData { npcId = sentence.npcId, playerSpeaking = sentence.player, sentence = text};
                    sentences.Enqueue(data);
                }
            }

            DisplayNextSentence();
        }

        public void DisplayNextSentence()
        {
            if(sentences.Count == 0)
            {
                EndDialogue();
                return;
            }

            DialogueData sentence = sentences.Dequeue();
            StopAllCoroutines();
            StartCoroutine(TypeSentence(sentence));
        }

        IEnumerator TypeSentence(DialogueData sentence)
        {
            if (!sentence.playerSpeaking)
                npcDialog.SetNpcVersion(sentence.npcId);
            npcDialog.SetSpeaking(!sentence.playerSpeaking);

            dialogueText.text = "";
            foreach (char letter in sentence.sentence.ToCharArray())
            {
                dialogueText.text += letter;
                yield return new WaitForSeconds(0.04f);
            }
        }

        void EndDialogue()
        {
            npcDialog.ShowOnScene(false);
            playerDialog.ShowOnScene(false);
            animator.SetBool("IsOpen", false);
            raycaster.enabled = true;
            player.AllowPlayerInput();
        }
    }
}
