﻿using System.Collections;
using UnityEngine;

namespace ep1
{
    public class DialogueTrigger : MonoBehaviour
    {
        public Dialogue[] dialogues = new Dialogue[1];
        public float delay = 0.0f;
        public MainSceneData data;

        public void TriggerDialogue()
        {
            foreach (var dialogue in dialogues)
            {
                StartCoroutine(Wait());
                FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
            }
            transform.gameObject.SetActive(false);
        }

        private IEnumerator Wait()
        {
            yield return new WaitForSeconds(delay);
        }

        void OnTriggerEnter2D(Collider2D collider)
        {
            if (data.stringBooleans["PlayerHasChangedClothes"])
                Destroy(this);
            else
                TriggerDialogue();
        }
    }
}
