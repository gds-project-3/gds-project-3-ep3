﻿using UnityEngine;

namespace ep1
{
    public class DialogBox : MonoBehaviour
    {
        public bool show = false;

        private Animator animator;
        private void Start()
        {
            animator = GetComponent<Animator>();
            SetShow(show);
        }

        public void SetShow(bool show)
        {
            this.show = show;
            animator.SetBool("IsOpen", show);
        }
    }
}