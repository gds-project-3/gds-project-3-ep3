﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [Tooltip("Name of main menu scene")]
    public string mainMenu;
    [Tooltip("Name of Episode 1 scene")]
    public string episode1;
    [Tooltip("Name of Episode 2 scene")]
    public string episode2;
    [Tooltip("Name of Episode 3 scene")]
    public string episode3;
    [Tooltip("Name of Episode 4 scene")]
    public string episode4;
    [Tooltip("Name of Episode 5 scene")]
    public string episode5;

    public void PlayGame ()
    {
        PlayEpisode1();
    }

    public void QuitGame ()
    {
        Application.Quit();
    }

    public void OpenMainMenu()
    {
        SceneManager.LoadScene(mainMenu);
    }

    public void PlayEpisode1()
    {
        SceneManager.LoadScene(episode1);
    }

    public void PlayEpisode2()
    {
        SceneManager.LoadScene(episode2);
    }

    public void PlayEpisode3()
    {
        SceneManager.LoadScene(episode3);
    }

    public void PlayEpisode4()
    {
        SceneManager.LoadScene(episode4);
    }

    public void PlayEpisode5()
    {
        SceneManager.LoadScene(episode5);
    }
}
