﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Common.Scripts.Player;
using Common.Scripts.BehaviourScripts;

namespace Common.Scripts.GameControllers
{
	public class GameController : MonoBehaviour {
		/// <summary>Static reference to the instance of our GameController</summary>
		public static GameController instance;

		/// <summary>Awake is called when the script instance is being loaded.</summary>
		void Awake()
		{
			// If the instance reference has not been set, yet, 
			if (instance == null)
			{
				// Set this instance as the instance reference.
				instance = this;
			}
			else if(instance != this)
			{
				// If the instance reference has already been set, and this is not the
				// the instance reference, destroy this game object.
				Destroy(gameObject);
			}

			// Do not destroy this object, when we load a new scene.
			DontDestroyOnLoad(gameObject);
		}

		public void RegisterScript( string scriptFile ) {
			if( !IsScriptInitialized( scriptFile ) ) 
				initializeScriptList.Add(scriptFile);
		}

		public bool IsScriptInitialized( string scriptFile ) {
			return initializeScriptList.Contains (scriptFile);
		}
			
		/// <summary>The player's current tags.</summary>
		public GameTagSet gameTagSet = new GameTagSet();

		/// <summary>Scene run arguments.</summary>
		public SceneRunParameters sceneRunParameteres;

		/// <summary>Script Loader Initializer List</summary>
		public List<string> initializeScriptList = new List<string>();

		/// <summary>Items Inventory</summary>
		public List<GameItem> invnentory = new List<GameItem>();
	}
}