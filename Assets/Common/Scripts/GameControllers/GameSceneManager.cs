﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Common.Scripts.Player;
using Common.Scripts.BehaviourScripts;

namespace Common.Scripts.GameControllers
{
	public class GameSceneManager : MonoBehaviour {

		public DialogBaloon playerDialogPanel;
		public Common.Scripts.Player.Player player;

		private string discussion_next_step = "";
		private GameObjectController discussion_object = null;

		public bool gameplayActive = true;

		// Use this for initialization
		void Start () {
			playerDialogPanel.ClearDialog ();

			if (commonGameController.sceneRunParameteres != null) {
				// Setup scene

				player.transform.position = commonGameController.sceneRunParameteres.startPosition;
			}
		}

		// Update is called once per frame
		void Update () {
		}

		public GameController commonGameController {
			get { return GameController.instance; }
		}

		public GameTagSet gameTagSet {
			get {
				if (commonGameController)
					return commonGameController.gameTagSet;
				else {
					Debug.Log ("GameScene Manager not initialized!");
					return null;
				}
			}
		}

		void OnMouseDown() {
			string message = "Time: "+Time.time.ToString() +  " current game tags:\n";
			gameTagSet.GameTags.ForEach( tag => message = message + tag.name + "," );
			Debug.Log ( message );

			string inventoryItems = "Inventory: ";
			commonGameController.invnentory.ForEach (gi => inventoryItems = inventoryItems + gi.name + ", ");
			Debug.Log (inventoryItems);

		}

		public float playerDistance( GameObject distanceToObject )
		{
			if (player == null)
				return 0f;

			return Vector3.Distance (player.transform.position, distanceToObject.transform.position);
		}

		public void addComment( string comment, float timeToHide, bool isNPC, BehaviourDiscussionOption bd = null ) 
		{
			playerDialogPanel.Comment (comment, timeToHide, isNPC, bd);
		}


		public void playerTell( string comment ) 
		{
			playerDialogPanel.Comment (comment, 3f, false);
			playerDialogPanel.SetWaitForClick (true);
		}

		public void setNPCDialogOption( bool npc_active, int npc_skin, int[] npc_skin_details ) {
			if (npc_active) { 
				if( npc_skin != null )
					playerDialogPanel.SetNPCAnimation (npc_skin);
				if (npc_skin_details != null) {
					playerDialogPanel.SetNPCAnimation (npc_skin_details);
				}
			}
			playerDialogPanel.SetNpcActive (npc_active);
		}

		public void NextDiscussionStep ( GameObjectController action_object, string next_step)
		{
			discussion_next_step = next_step;
			discussion_object = action_object;
			playerDialogPanel.SetWaitForClick (true);
		}

		public bool OnDialogNextStep () {
			if( discussion_next_step == "discussion_end" ) return true;

			if (discussion_next_step != "" && discussion_object != null) {
				discussion_object.startDiscussion (discussion_next_step);
				return false;
			}

			return true;    // close conversation
		}

		public bool OnDialogSelectOption (BehaviourDiscussionOption discussionOption) {
			discussion_object.selectDiscussionOption (discussionOption);
			//if( discussion_next_step == "discussion_end" ) return true;
			return false;
		}

		public void StopGameplay() 
		{
			gameplayActive = false;
			player.BlockPlayerInput();
		}

		public void RestoreGameplay()
		{
			gameplayActive = true;
			player.AllowPlayerInput();
		}

		public void OnStartAnimation() {
			playerDialogPanel.OnAnimationStart ();
		}
			
		public void OnFinishAnimation() {
			playerDialogPanel.OnAnimationEnd ();
		}

		/*************************************************/

		public void commonPlayerPickAudio() {
			if (player == null)
				return;
			PlayerAudio pa = player.GetComponent<PlayerAudio> ();
			if( pa != null )
				pa.PlayPickItem ();
		}

		/*************************************************/

		public void LoadScene( string scene, SceneRunParameters param ) {
			commonGameController.sceneRunParameteres = param;
			SceneManager.LoadScene(scene);
		}
	}
}