﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Scripts.GameControllers
{
	public class GameTagSet  {

		List<GameTag> tagSet;

		public GameTagSet() {
			tagSet = new List<GameTag>();
		}


		public void addTag( GameTag tag ) {
			tagSet.Add(tag);
		}

		public bool removeTag( GameTag tag ) {
			return tagSet.Remove (tag);
		}

		public bool checkTagExists( GameTag findtag ) {
			return checkTagExists (findtag.name);
		}

		public bool checkTagExists( string findtag ) {
			if (tagSet.Find (tag => tag.name == findtag) != null) {
				return true;
			}
			return false;
		}

		public bool checkTagsExists( List<GameTag> tags ) {
			if (tags == null)
				return true;

			foreach( GameTag findTag in tags ) {
				if( false == checkTagExists( findTag ) )
					return false;
			}
			return true;
		}

		public void AddTagsFromString (string[] add_tags) {
			if (add_tags == null)
				return;

			foreach (var stag in add_tags) {
				GameTag gt = new GameTag (stag);
				if (!checkTagExists (gt)) addTag (gt);
			}
		}

		public void RemoveTagsFromString (string[] rem_tags) {
			if (rem_tags == null)
				return;

			foreach (var rtag in rem_tags) {
				GameTag gt = new GameTag (rtag);
				if (checkTagExists (gt)) removeTag (gt);
			}
		}

		public bool checkTagsExists( string[] tags ) {
			if (tags == null)
				return true;
			
			foreach (string tag in tags) {
				if( false == checkTagExists( tag ) )
					return false;
				Debug.Log (tag + "=true");
			}
			return true;
		}

		public List<GameTag> GameTags {
			get {
				return tagSet;
			}
		}

	}
}