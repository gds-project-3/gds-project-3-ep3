﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Common.Scripts.BehaviourScripts;
using UnityEngine.EventSystems;
using ep2.Scripts;

namespace Common.Scripts.GameControllers
{
	public class DialogBaloon : MonoBehaviour, IPointerClickHandler {


		private NPC_dialog npcDialog;
		private Animator _dialog_animator = null;
		private bool dialogShow = false;
		private bool dialogTell = false;
		private float hideTime = 0f;

		private bool showNPC = false;
		private bool isSpeakingNPC = false;
		private int skinNPC = 6;
		private int[] skinDetails = null;

		private int currentLine = 0;
		private int visibleLines = 7;

		private DialogBaloonState state = DialogBaloonState.none;

		public DialogBaloonTextLine[] DialogTextLines;
		public Text clickTextLabel;
		public GameSceneManager gameSM;

		public float clickIndicatorFlashSpeed = 4f;
		public float npcSpeakingTime = 1f;

		// Use this for initialization
		void Start () {
			Initialize ();
			Hide ();
		}
		
		// Update is called once per frame
		void Update () {
			if (dialogShow && state == DialogBaloonState.timeClose && Time.time > hideTime) {
				Hide ();
				dialogShow = false;
				state = DialogBaloonState.none;
			}

			if (state == DialogBaloonState.waitForClick) {
				float d = (Mathf.Sin (Time.time / clickIndicatorFlashSpeed) * 0.5f) + 0.5f;
				clickTextLabel.color = Color.green * d + Color.white * (1f - d);
			} else {
				clickTextLabel.color = Color.clear;
			}

			if (isSpeakingNPC && Time.time > npcSpeakingTime) {
				// Stop NPC Speaking
				isSpeakingNPC = false;
				npcDialog.Speaking (isSpeakingNPC);
			}
		}

		private void Initialize() {
			visibleLines = DialogTextLines.Length;

			GameObject dialogPlayerAnimation = transform.Find("PLAYER_DIALOG").gameObject;
			if (dialogPlayerAnimation != null) {
				_dialog_animator = dialogPlayerAnimation.GetComponent<Animator> ();
			}

			npcDialog = transform.Find ("NPC_dialog").gameObject.GetComponent<NPC_dialog> ();

		}


		public void Show() {

			if (_dialog_animator) {
				_dialog_animator.SetBool ("show_dialog", true);
			}
			ShowNPCAnimation (true);

			GetComponent<CanvasGroup> ().alpha = 1f;
			dialogShow = true;

			if (state == DialogBaloonState.none)
				state = DialogBaloonState.timeClose;
		}

		public void Hide() {
			GetComponent<CanvasGroup> ().alpha = 0f;
			ClearDialog ();
			if (_dialog_animator) {
				_dialog_animator.SetBool ("show_dialog", false);
			}
			ShowNPCAnimation (false);
			dialogShow = false;
			state = DialogBaloonState.none;
		}
			
		public void Comment( string comment, float timeToHide, bool isNPC, BehaviourDiscussionOption dialogOption = null ) {
			hideTime = Time.time + timeToHide;
			if (!dialogShow) Show ();
			addLine (comment, isNPC, dialogOption);
			if (state == DialogBaloonState.none)
				state = DialogBaloonState.timeClose;
		}

		public void ClearDialog() {
			currentLine = 0;
			for (int i = 0; i < DialogTextLines.Length; i++)
				ResetDialogLine (i);
		}

		public void addLine( String text, bool isNPC = false, BehaviourDiscussionOption dialogOption = null ) {
			string[] lines = text.Split( new string[] {"\n"}, StringSplitOptions.None );
			addLines (lines, isNPC, dialogOption);
			if (isNPC)
				NPCSpeaking ();
		}

		public void addLines( string[] lines, bool isNPC = false, BehaviourDiscussionOption dialogOption = null ) {

			foreach( string text in lines ) {
				string t = text;
				if (currentLine == visibleLines) {			
					ShiftDialogUp ();
				}

				if (dialogOption != null) {
					t = "   >>> " + t;
					state = DialogBaloonState.waitForDecision;
				}

				DialogTextLines [currentLine].text.text = t;
				DialogTextLines [currentLine].NPCLine = isNPC;
				DialogTextLines [currentLine].DiscussionOption = dialogOption;

				currentLine++;
			}
		}



		private void ResetDialogLine(int i) {
			if (DialogTextLines [i].text != null) {
				DialogTextLines [i].text.text = "";
				DialogTextLines [i].NPCLine = false;
				DialogTextLines [i].DiscussionOption = null;
			}
		}

		private void ShiftLineUp(int i) {
			if (i == 0)
				return;

			DialogTextLines [i-1].text.text = DialogTextLines [i].text.text;
			DialogTextLines [i-1].NPCLine = DialogTextLines [i].NPCLine;
			DialogTextLines [i-1].DiscussionOption = DialogTextLines [i].DiscussionOption;
		}

		private void RemoveLine(int rm) {
			for (int i = rm; i < visibleLines-1; i++)
				ShiftLineUp (i+1);
			ResetDialogLine (visibleLines-1);
			currentLine--;
		}

		private void ShiftDialogUp() {
			for (int i = 1; i < visibleLines; i++)
				ShiftLineUp (i);
			ResetDialogLine (visibleLines-1);
			currentLine--;
		}

		public void SetWaitForClick( bool _waitForClick ) {

			if (state == DialogBaloonState.waitForAnimation)
				return;

			if (_waitForClick) {
				state = DialogBaloonState.waitForClick;
				gameSM.StopGameplay();
			} else {
				gameSM.RestoreGameplay();
				state = DialogBaloonState.timeClose;
			}
		}

		public void OnPointerClick(PointerEventData ED) {

			Debug.Log ("Click baloon!");

			if (state == DialogBaloonState.waitForClick) {
				if (gameSM.OnDialogNextStep ()) {
					Hide ();
					SetWaitForClick (false);
				}
			}
		}

		public void OnAnimationEnd() {
			state = DialogBaloonState.waitForClick;
			SetWaitForClick( true );
			if (gameSM.OnDialogNextStep ()) {
				Hide ();
				SetWaitForClick (false);
			}
		}

		public void OnAnimationStart() {
			state = DialogBaloonState.waitForAnimation;
		}

		public void ClickDialogLine ( int lineNumber, BehaviourDiscussionOption discussionOption) {

			if (state == DialogBaloonState.waitForAnimation)
				return;
			
			// TODO: DisableOptionsForClick

			for (int i = 0; i < visibleLines; i++) {
				if (DialogTextLines [i].DiscussionOption != null) {
					RemoveLine (i);
					i--;
				}
			}

			if (gameSM.OnDialogSelectOption (discussionOption)) {
				Hide ();
				SetWaitForClick (false);
			}
		}


		/*************************************************************/
		// NPC Animation

		public void SetNPCAnimation( int skin ) {
			skinNPC = skin;
			skinDetails = null;
		}

		public void SetNPCAnimation( int[] skin_details ) {
			skinDetails = skin_details;
			skinNPC = 0;
		}

		public void SetNpcActive( bool npcActive ) {
			showNPC = npcActive;
		}

		public void ShowNPCAnimation( bool show ) {
			if (showNPC) {
				if (skinDetails != null)
					npcDialog.SkinDetails = new List<int> (skinDetails);
				else
					npcDialog.Skin = skinNPC;
				npcDialog.Show (show);
			}
		}

		public void NPCSpeaking() {
			npcSpeakingTime = Time.time + 1f;
			isSpeakingNPC = true;
			npcDialog.Speaking (isSpeakingNPC);
		}
	}

	public enum DialogBaloonState 
	{
		none = 0,
		timeClose = 1,
		waitForClick = 2,
		waitForDecision = 3,
		waitForAnimation = 4
	}
}
