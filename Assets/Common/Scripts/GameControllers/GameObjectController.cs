﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common.Scripts.BehaviourScripts;
using SpriteGlow;

namespace Common.Scripts.GameControllers
{
	public class GameObjectController : MonoBehaviour {

		public string jsonScriptName;
		BehaviourScript jsonScript = null;
		GameSceneManager gameSM = null;
		public bool objectEnabled = false;
		private SpriteGlowEffect _spriteGlowEffect;
		public float GlowBrightness = 2;
		public int GlowOutlineWidth = 10;
		public bool detectCollision = false;

		private int actionOnFinishAnimationOrder = -1;

		public Sprite[] sprites;
		public AudioClip[] sfx;


		// Use this for initialization
		void Start () {

			// Find game controller

			var c = FindObjectOfType<GameSceneManager> ();
			if (c == null) {
				Debug.Log ("Game Controller not exists!");
				return;
			}
			gameSM = c;

			// Load json script

			if (jsonScriptName.CompareTo ("") == 0) {
				Debug.Log ("Script for object "+this.name+" not exists!");
				return;
			}
			BehaviourScriptLoader scriptLoader = new BehaviourScriptLoader (jsonScriptName);
			jsonScript = scriptLoader.LoadScript ();

			InitializeScript ();

			// Add glow effect
			_spriteGlowEffect = GetComponent<SpriteGlowEffect>();
			if (_spriteGlowEffect == null) {
				if( GetComponent<SpriteRenderer>() != null )
					_spriteGlowEffect = gameObject.AddComponent<SpriteGlowEffect> ();
			}
			LightOff ();


			ActionOnLoad ();

		}

		// Update is called once per frame
		void Update () {
			/*if (Input.GetMouseButtonDown(0)) {
				Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

				RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
				if (hit.collider != null) {
					Debug.Log(hit.collider.gameObject.name);
				
				}
			} */
		}

		// *********************************************************
		// *** Initialize script

		private void InitializeScript() {

			if (!gameSM.commonGameController.IsScriptInitialized (jsonScriptName)) {

				gameSM.commonGameController.RegisterScript (jsonScriptName);

				// Initialize object on scene
				if (jsonScript.start_game_tags != null && jsonScript.start_game_tags.Length > 0) {
					gameSM.gameTagSet.AddTagsFromString (jsonScript.start_game_tags);
				}
			}
		}

		private void ActionOnLoad() {
			if (jsonScript.action_on_load >= 0) {
				BehaviourAction startActon = getCurrentAction (null,jsonScript.action_on_load);
				if (startActon == null)
					return;

				/*if (!gameSM.gameTagSet.checkTagsExists (startActon.exists_tags))
					return;
					*/

				runAction (startActon);
			}
		}

		// *********************************************************
		// *** Mouse and Trigger Events

		void OnMouseDown() {
			if( gameSM.gameplayActive )
				runScript ( null );
		}

		private void OnMouseEnter()
		{
			LightOn();
		}

		private void OnMouseExit()
		{
			LightOff();
		}

		void OnTriggerEnter2D(Collider2D coll) { 
			if (coll.gameObject.tag == "Player" && detectCollision == true) {
				if (gameSM.gameplayActive)
					runScript (null);	
			}

		}

		// ********************************************************
		// *** Run Script

		public void runScript( GameTag withTag ) {
			if ( jsonScript==null ) {
				Debug.Log ("Script error!");
				return;
			}
			gameSM.setNPCDialogOption (jsonScript.npc_active, jsonScript.npc_skin, jsonScript.npc_skin_details);
			runDefaultAction (withTag);
		}


		// ********************************************************
		// *** Actions 


		public void runDefaultAction(GameTag withTag) {
			BehaviourAction action = getCurrentAction (withTag);
			if (action != null)
				runAction (action);
			else
				Debug.Log ("No action detected!");
		}

		private void runAction( BehaviourAction action ) {
			var distance = gameSM.playerDistance (this.gameObject);
			Debug.Log (jsonScript.refobject+" run action="+action.order+" distance="+distance.ToString());

			if ( action.max_distance != null && action.max_distance != 0 && distance > action.max_distance) {
				gameSM.addComment ("Jestem za daleko, aby to zrobić", 3f, false);
				return;
			}

			if ( action.min_distance != null && distance < action.min_distance) {
				gameSM.addComment ("Jestem za blisko!", 3f, false);
				return;
			}

			if (action.comment != null ) {
				gameSM.addComment (action.comment, 3f, true );
			}

			if (action.tell_player != null ) {
				gameSM.playerTell(action.tell_player);
			}

			PlaySound (action.play_sfx);
			EnableAndDisableGameObjects (action.enable_gameobjects, action.disable_gameobjects);
			SetSprite (action.set_sprite);
			actionOnFinishAnimationOrder = action.action_on_finish_animation;
			RunAnimation (action.start_animation, true);
			MoveObjcetTo (action.move_object_to);

			if (action.get_item == true) {
				GetThisItem ();
			}

			if (action.get_item_sfx == true) {
				gameSM.commonPlayerPickAudio ();
			}

			if (action.operation == "discussion" ) {
				startDiscussion( action );
			}

			if (action.operation == "goto scene") {
				SceneRunParameters srp = null;
				if (action.start_pos != null && action.start_pos.Length == 2) {
					srp = new SceneRunParameters ();
					srp.startPosition = new Vector3 (action.start_pos[0], action.start_pos[1], 0f);
				}
				GotoScene (action.data, srp);
			}

			if (action.operation == "disable") {
				this.gameObject.SetActive (false);
			}

			if (action.operation == "enable") {
				this.gameObject.SetActive (true);
			}

			processTags (action.add_tags, action.remove_tags);
		}

		private BehaviourAction getCurrentAction( GameTag withTag, int startFindAction = -1 ) {

			var sortActions = new List<BehaviourAction> (jsonScript.actions);

			sortActions.Sort ((a, b) => {
				return a.order - b.order;
			});

			if (sortActions.Count == 0)
				return null;
			int startAction = sortActions [0].order;

			if (startFindAction == -1) {
				startAction = sortActions [0].order;
				if (jsonScript.random_start_actions != null) {
					startAction = Random.Range (0, jsonScript.random_start_actions.Length);
					startAction = jsonScript.random_start_actions [startAction];
				}
			} else {
				startAction = startFindAction;
			}
				int startIndex = sortActions.IndexOf (sortActions.Find (a => a.order == startAction));
			
			for (int i = startIndex; i < sortActions.Count; i++) {
				BehaviourAction a = sortActions [i];
				if (withTag != null) {
					if (a.action_with_tag != withTag.name)
						continue;
				}

				if (!gameSM.gameTagSet.checkTagsExists (a.exists_tags)) {
					continue;
				}
				return a;
			}

			/*foreach (BehaviourAction a in sortActions) {
				if (withTag != null) {
					if (a.action_with_tag != withTag.name)
						continue;
				}

				if (!gameSM.gameTagSet.checkTagsExists (a.exists_tags)) {
					continue;
				}


				return a;
			} */

			return null;
		}


		private BehaviourAction findAction( int order ) {
			if (jsonScript == null || jsonScript.actions == null)
				return null;
			
			foreach (BehaviourAction action in jsonScript.actions)
				if (action.order == order)
					return action;

			return null;
		}

		/*********************************************************************/
		// Tags

		private void processTags(string[] add_tags, string[] remove_tags) {
			if (remove_tags != null && remove_tags.Length > 0) {
				CheckIfItemRemoveFromInventory (remove_tags);
				gameSM.gameTagSet.RemoveTagsFromString (remove_tags);
			}
				
			if (add_tags != null && add_tags.Length > 0) {
				gameSM.gameTagSet.AddTagsFromString (add_tags);
			}
		}


		/*********************************************************************/
		// Discussions

		public void startDiscussion( BehaviourAction action )
		{
			startDiscussion (action.data);
		}

		public void startDiscussion( string disscusion_id )
		{
			BehaviourDiscussion discussion = findDiscussion (disscusion_id);
			if (discussion == null) 
			{
				Debug.Log ("Discussion " + disscusion_id + " not found");
				return;
			}

			runDiscussionStep (discussion);
		}
			
		private BehaviourDiscussion findDiscussion( string discussion_id )
		{
			foreach (BehaviourDiscussion dis in jsonScript.discussions) {
				if (dis.discussion_step == discussion_id)
					return dis;
			}

			return null;
		}
			
		public void runDiscussionStep ( BehaviourDiscussion discussion ) 
		{
			if (discussion == null)
				return;

			PlaySound (discussion.play_sfx);
			SetSprite (discussion.set_sprite);
			processTags(discussion.add_tags, discussion.remove_tags);
			EnableAndDisableGameObjects (discussion.enable_gameobjects, discussion.disable_gameobjects);
			RunAnimation (discussion.start_animation);
			RunActionObject (discussion.action_gameobjects);
			GotoScene (discussion.goto_scene,null);


			if (discussion.get_item == true) {
				GetThisItem ();
			}

			if (discussion.get_item_sfx == true) {
				gameSM.commonPlayerPickAudio ();
			}

			if (discussion.first == null || discussion.first == "npc") {
				addDiscussionNPC (discussion);
				addDiscussionPlayer (discussion);
			}

			if (discussion.first == "hero") {
				addDiscussionPlayer (discussion);
				addDiscussionNPC (discussion);
			}
				
			if( discussion.next_step != null )
				gameSM.NextDiscussionStep ( this, discussion.next_step);

		}

		private void addDiscussionNPC (BehaviourDiscussion discussion)
		{
			if( discussion.npc_side != null && discussion.npc_side  != "" )
				gameSM.addComment (discussion.npc_side, 3f, true );
		}

		private void addDiscussionPlayer (BehaviourDiscussion discussion)
		{
			if( discussion.hero_side != null && discussion.hero_side  != "" )
				gameSM.playerTell(discussion.hero_side);

			if (discussion.hero_ask != null) {
				foreach (BehaviourDiscussionOption bdo in discussion.hero_ask) {
					
					if( bdo.exists_tags != null ) {
						if (!gameSM.gameTagSet.checkTagsExists (bdo.exists_tags))
							continue;
					}

					gameSM.addComment (bdo.option, 10f, false, bdo);
				}
			}
		}

		public void selectDiscussionOption (BehaviourDiscussionOption discussionOption) {
			if (discussionOption == null)
				return;

			gameSM.playerTell (discussionOption.option);
			PlaySound (discussionOption.play_sfx);
			SetSprite (discussionOption.set_sprite);
			processTags(discussionOption.add_tags, discussionOption.remove_tags);
			EnableAndDisableGameObjects (discussionOption.enable_gameobjects, discussionOption.disable_gameobjects);
			RunAnimation (discussionOption.start_animation);
			GotoScene (discussionOption.goto_scene, null);

			if (discussionOption.get_item == true) {
				GetThisItem ();
			}

			if (discussionOption.get_item_sfx == true) {
				gameSM.commonPlayerPickAudio ();
			}

			if (discussionOption.next_step != null) {
				startDiscussion (discussionOption.next_step);
				//gamec.NextDiscussionStep (this, discussionOption.next_step);
			}
		}


		/*********************************************************************/
		// Inventory

		private void GetThisItem() {
			Debug.Log ("get object " + jsonScript.refobject);
			AddObjectToInventory ();
			HideObject ();
			gameObject.SetActive (false);
			gameSM.commonPlayerPickAudio ();
			//StartCoroutine( DestroyMyself (2f));
		}

		private void AddObjectToInventory() {
			GameItem gi = new GameItem();
			gi.name = jsonScript.refobject;
			gi.gameTagItem = new GameTag (jsonScript.game_item_tag);
			gi.sprite2d = new Sprite ();
			gameSM.commonGameController.invnentory.Add (gi);
		}

		private void CheckIfItemRemoveFromInventory( string[] remove_tags ) {
			GameItem rmItem = null;

			foreach (string remove_tag in remove_tags) {
				rmItem = null;
				foreach (GameItem gi in gameSM.commonGameController.invnentory) {
					if (gi.gameTagItem.name == remove_tag) {
						rmItem = gi;
						break;
					}
				}
				if (rmItem != null)
					gameSM.commonGameController.invnentory.Remove (rmItem);
			}
		}

		//*************************************************************
		// Disable/enable objects on scene

		private void EnableAndDisableGameObjects( string[] toEnable, string[] toDisable ) {
			if (toEnable != null) {
				foreach (string go in toEnable) {
					GameObject tr = FindInactiveObject (go);
					if (tr == null)
						Debug.Log ("Cant find and enable " + go);
					else {
						tr.SetActive (true);
					}
				}
			}

			if (toDisable != null) {
				foreach (string go in toDisable) {
					GameObject tr = GameObject.Find (go);
					if (tr == null)
						Debug.Log ("Cant find and disable " + go);
					else {
						tr.SetActive (false);
					}
				}
			}
		}

		private void RunActionObject( string[] toAction ) {
			if (toAction != null) {
				foreach (string go in toAction) {
					GameObject tr = GameObject.Find (go);
					if (tr == null)
						Debug.Log ("Cant find and run action on " + go);
					else {
						GameObjectController goc = tr.GetComponent<GameObjectController> ();
						goc.runDefaultAction (null);
					}
				}
			}
		}

		private GameObject FindInactiveObject( string path ) {

			int splitIndex = path.LastIndexOf ("/");
			if (splitIndex == -1)
				return null;
			string parent = path.Substring (0, splitIndex);
			string name = path.Substring (splitIndex+1);

			GameObject parentGo = GameObject.Find (parent);
			if (parentGo == null)
				return null;
			
			GameObject findGo = FindInChildrenIncludingInactive( parentGo, name );
			return findGo;
		}

		public static GameObject FindInChildrenIncludingInactive(GameObject go, string name)
		{

			for (int i = 0; i < go.transform.childCount; i++)
			{
				if (go.transform.GetChild(i).gameObject.name == name) return go.transform.GetChild(i).gameObject;
				GameObject found = FindInChildrenIncludingInactive(go.transform.GetChild(i).gameObject, name);
				if (found != null) return found;
			}

			return null;  //couldn't find crap
		}


		//*************************************************************
		// Play Sound

		private void PlaySound(int sound) {
			if (sound != null && sound >=0 ) {
				if (sound >= sfx.Length) {
					Debug.Log ( jsonScript.refobject+ " sfx index is out of range");
					return;
				}

				AudioSource asrc = GetComponent<AudioSource> ();
				if (asrc == null || sfx[sound]==null)
					return;
				
				asrc.clip = sfx [sound];
				asrc.Play ();
			}
		}

		//*************************************************************
		// Set sprite texture

		private void SetSprite(int spriteIndex) {
			if (spriteIndex != null && spriteIndex >=0 ) {
				if (spriteIndex >= sprites.Length) {
					Debug.Log ( jsonScript.refobject+ " sprite index is out of range");
					return;
				}

				SpriteRenderer sprr = GetComponent<SpriteRenderer> ();
				if (sprr == null || sprites[spriteIndex]==null)
					return;
				sprr.sprite = sprites [spriteIndex];
			}
		}

		//*************************************************************
		// Animation event

		private void RunAnimation(int animation, bool fromAction=false) {

			if (animation < 0)
				return;

			Animator anim = GetComponent<Animator>();
			if (anim == null)
				return;

			anim.SetInteger ("animation", animation);
			if( fromAction == false ) 
				gameSM.OnStartAnimation ();
		}
			
		private void FinishAnimation() {
			Animator anim = GetComponent<Animator>();
			if (anim == null)
				return;

			anim.SetInteger ("animation", -1);
			gameSM.OnFinishAnimation ();
		}

		private void ActionOnFinishAnimation() {
			Animator anim = GetComponent<Animator>();
			if (anim == null)
				return;
			anim.SetInteger ("animation", -1);
			if (actionOnFinishAnimationOrder >= 0 ) {
				Debug.Log ("run action" + actionOnFinishAnimationOrder.ToString ());
				BehaviourAction nextActon = getCurrentAction (null,actionOnFinishAnimationOrder);
					if (nextActon == null)
						return;

				/*	if (!gameSM.gameTagSet.checkTagsExists (nextActon.exists_tags))
						return;
					*/
					runAction (nextActon);
			}
		}

		//***********************************************************
		// *** Goto Scene

		private void GotoScene( string sceneName, SceneRunParameters srp ) {
			if( sceneName != "" )
				gameSM.LoadScene (sceneName,srp);
		}



		//************************************************************//
		// Object actions

		private void HideObject() {
			Renderer rend = GetComponent<Renderer> ();
			rend.enabled = false;
		}

		private void ShowObject() {
			Renderer rend = GetComponent<Renderer> ();
			rend.enabled = true;
		}

		private void MoveObjcetTo ( float[] newPos ) {
			if (newPos == null || newPos.Length != 2)
				return;

			transform.localPosition = new Vector3 (newPos [0], newPos [1], 0);
			}

		public void LightOn()
		{
			if (_spriteGlowEffect) {
				_spriteGlowEffect.OutlineWidth = GlowOutlineWidth;
				_spriteGlowEffect.GlowBrightness = GlowBrightness;
			}
		}

		private void LightOff()
		{
			if (_spriteGlowEffect) {
				_spriteGlowEffect.OutlineWidth = 0;
			}
		}

		IEnumerator DestroyMyself(float waitSeconds) {
			yield return new WaitForSeconds(waitSeconds);
			Destroy (this.gameObject);
		}


	}
}