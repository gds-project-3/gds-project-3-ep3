﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Scripts.GameControllers
{
	public class GameTag {

		public string name;
		public bool game_object;

		public GameTag( string name ) {
			this.name = name;
		}

		public override bool Equals(object o) {
			var gt = o as GameTag;
			if (gt == null)
				return false;

			return this.name.Equals(gt.name);
		}

	}
}