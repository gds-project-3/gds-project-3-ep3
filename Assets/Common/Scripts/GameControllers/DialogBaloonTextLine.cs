﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Common.Scripts.BehaviourScripts;

namespace Common.Scripts.GameControllers 
{
	public class DialogBaloonTextLine : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler  {

		private BehaviourDiscussionOption discussionOption = null;
		private Text textComponent;
		private bool isNPCLine = false;
		private bool isActiveLine = false;
		private int lineNumber = -1;
		private DialogBaloon dialogBaloon = null;
		private bool selected = false;

		public float clickIndicatorFlashSpeed = 4f*Mathf.PI;

	

		// Use this for initialization
		void Start () {
			textComponent = GetComponent<Text> ();
			SetLineStyle ();
			dialogBaloon = transform.parent.gameObject.GetComponent<DialogBaloon>();
		}
		
		// Update is called once per frame
		void Update () {
			if (discussionOption != null && selected) {
				float d = (Mathf.Sin (Time.time * clickIndicatorFlashSpeed) * 0.5f) + 0.5f;
				textComponent.color = Color.green * d + Color.white * (1f-d);
			}

		}

		public BehaviourDiscussionOption DiscussionOption {
			set {
				discussionOption = value;
				SetLineStyle ();
			}
			get {
				return discussionOption;
			}
		}

		public int LineNumber {
			set {
				lineNumber = value;
			}
			get {
				return lineNumber;
			}
		}

		public bool NPCLine {
			set {
				isNPCLine = value;
				SetLineStyle ();
			}
			get {
				return isNPCLine;
			}
		}

		public Text text {
			get {
				return textComponent;
			}
		}

		public void OnPointerEnter( PointerEventData eventData ) {
			if (discussionOption != null) {
				selected = true;
			}
		}

		public void OnPointerExit( PointerEventData eventData ) {
			if (discussionOption != null) {
				selected = false;
				textComponent.color = Color.green;
			}
		}

		public void OnPointerClick( PointerEventData eventData ) {
			if (discussionOption != null && dialogBaloon != null) {
				dialogBaloon.ClickDialogLine (lineNumber, discussionOption);
			} else {
				// PassUp
				dialogBaloon.OnPointerClick(eventData);
			}
		}

		private void SetLineStyle() {
			textComponent.alignment = TextAnchor.MiddleLeft;
			textComponent.color = Color.white;

			if (isNPCLine) {
				textComponent.alignment = TextAnchor.MiddleRight;
				textComponent.color = Color.yellow;
			}

			if (discussionOption != null) {
				textComponent.alignment = TextAnchor.MiddleLeft;
				textComponent.color = Color.green;
				textComponent.fontStyle = FontStyle.Italic;
			}
		}

	}
}
