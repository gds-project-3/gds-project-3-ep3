﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common.Scripts.GameControllers;

namespace Common.Scripts.BehaviourScripts
{
		
	public class GameItem  {

		public string name = "";
		public GameTag gameTagItem = null;
		public Sprite sprite2d = null;
	}
}