﻿using System;

namespace Common.Scripts.BehaviourScripts
{

	[System.Serializable]
	public class BehaviourDiscussionOption
	{
		public string option;
		public string next_step;
		public string goto_scene="";
		public string[] exists_tags;
		public string[] remove_tags;
		public string[] add_tags;
		public string[] enable_gameobjects = null;
		public string[] disable_gameobjects = null;
		public bool get_item = false;
		public bool get_item_sfx = false;
		public int play_sfx = -1;
		public int set_sprite = -1;
		public int start_animation = -1;

		public BehaviourDiscussionOption ()
		{
		}
	}
}