﻿using System;
using UnityEngine;

namespace Common.Scripts.BehaviourScripts
{
	[System.Serializable]
	public class BehaviourScript
	{
		public string refobject = "";
		public string[] start_game_tags = null;
		public bool game_item = false;
		public string game_item_tag = null;
		public string game_item_spriteName = null;
		public int npc_skin = 0;
		public bool npc_active = false;
		public int[] npc_skin_details = null;
		public int[] random_start_actions = null;
		public int action_on_load = -1;
		public BehaviourAction[] actions = null;
		public BehaviourDiscussion[] discussions = null;
	}
}