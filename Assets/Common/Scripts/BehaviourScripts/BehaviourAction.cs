﻿using System;

namespace Common.Scripts.BehaviourScripts
{
	[System.Serializable]
	public class BehaviourAction
	{
		public int order;
		public string action;
		public string operation;
		public string comment;
		public string tell_player;
		public string[] exists_tags;
		public string action_with_tag;
		public string[] remove_tags;
		public string[] add_tags;
		public string data;
		public float min_distance;
		public float max_distance;
		public float[] start_pos;
		public string[] enable_gameobjects = null;
		public string[] disable_gameobjects = null;
		public bool get_item = false;
		public bool get_item_sfx = false;
		public int play_sfx = -1;
		public int set_sprite = -1;
		public int start_animation = -1;
		public int action_on_finish_animation = -1;
		public float[] move_object_to;

		public BehaviourAction ()
		{
		}
	}
}