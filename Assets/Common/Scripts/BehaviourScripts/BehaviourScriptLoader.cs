﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace Common.Scripts.BehaviourScripts
{
	public class BehaviourScriptLoader 
	{
		public string scriptFile;


		public BehaviourScriptLoader( string fileName ) {
			this.scriptFile = fileName;
		}

		public BehaviourScript LoadScript()
		{
			BehaviourScript bScipt = null;
			string filePath = Path.Combine (Application.streamingAssetsPath, scriptFile);
			Debug.Log (filePath);

			if (File.Exists (filePath)) {
				string scriptText = File.ReadAllText (filePath);
				bScipt = JsonUtility.FromJson<BehaviourScript> (scriptText);
			} else {
				Debug.Log (filePath + " not Exists");
			}

			Debug.Log (JsonUtility.ToJson(bScipt));
			return bScipt;
		}
			
	}

}