﻿using System.Collections.Generic;
using Common.Scripts.Entity;
using UnityEngine;

namespace Common.Scripts.Trigger
{
    public class InteractionSwitchTrigger : MonoBehaviour
    {
        public List<DialogueSwitchDefinition> DialogueSwitchDefinitions = new List<DialogueSwitchDefinition>();
        public List<PopupSwitchDefinition> PopupSwitchDefinitions = new List<PopupSwitchDefinition>();

        private void OnTriggerEnter2D(Collider2D other)
        {
            var player = other.GetComponent<Player.Player>();
            if (player == null) return;
            Debug.Log("Player detected on " + transform.name + ": switching interactions");
            PopupSwitchDefinitions.ForEach(d =>
            {
                Debug.Log("Entity: " + d.Popup.transform.name + ", new popup node: " + d.NewPopupNode);
                d.Popup.PopupNode = d.NewPopupNode;
            });
            DialogueSwitchDefinitions.ForEach(d =>
            {
                Debug.Log("Entity: " + d.Dialogue.transform.name + ", new dialogue node: " + d.NewDialogueNode);
                d.Dialogue.DialogueNode = d.NewDialogueNode;
            });
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.matrix = Matrix4x4.TRS(transform.position, Quaternion.identity, new Vector3(1, 1, 0));
            // Need to draw at position zero because we set position in the line above
            Gizmos.DrawWireCube(Vector3.zero, GetComponent<Collider2D>().bounds.size);
        }
    }
}