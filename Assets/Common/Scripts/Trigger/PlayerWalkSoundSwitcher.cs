﻿using Common.Scripts.Player;
using UnityEngine;

namespace Common.Scripts.Trigger
{
    public class PlayerWalkSoundSwitcher : MonoBehaviour
    {
        public int WalkSoundIndex;

        private void OnTriggerEnter2D(Collider2D other)
        {
            var playerAudio = other.GetComponent<PlayerAudio>();
            if (playerAudio == null) return;
            Debug.Log("Player detected on " + transform.name + ": switching walk sound");
            playerAudio.ChangeWalkSound(WalkSoundIndex);
        }

    }
}