﻿using Common.Scripts.Dialogue.Command;
using UnityEngine;

namespace Common.Scripts.Trigger
{
    public class SetPositionTrigger : MonoBehaviour
    {
        public Transform ToChange;
        public Transform TargetPosition;

        private void OnTriggerEnter2D(Collider2D other)
        {
            Debug.Log("Player detected on " + transform.name + ", moving: " + ToChange.name + ", to: " + TargetPosition.position);
            ToChange.position = TargetPosition.position;
        }
    }
}