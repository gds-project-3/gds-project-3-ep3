﻿using UnityEngine;

namespace Common.Scripts
{
	public class UpdateSortingLayerForCharacters : MonoBehaviour {
		private void LateUpdate()
		{
			var spriteRenderers = FindObjectsOfType<SpriteRenderer>();
			foreach (var sr in spriteRenderers)
			{
				if (sr.sortingLayerName == "Characters")
				{
					sr.sortingOrder = (int) Camera.main.WorldToScreenPoint(sr.bounds.min).y * -1;
				}
			}
		}
	}
}
