﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Common.Scripts.Entity
{
    public class Item : Interactable
    {
        public string ItemName = "";
        public GameObject item;

        protected override bool OnInteract(Player.Player player)
        {
            GameObject obj = GameObject.Find("ep5_Equipment");
            if (obj != null)
            {
                ep5_Inventory eqController = obj.GetComponent<ep5_Inventory>();

                if (eqController.AddItem(item))
                    Destroy(gameObject);
                else
                    GameObject.Find("Text").GetComponent<Text>().text = "Brak miejsca";
            }

            player.AddItem(this);
            gameObject.SetActive(false);
            gameObject.transform.SetParent(player.transform);
            return true;
        }
    }
}