﻿namespace Common.Scripts.Entity
{
    public class Dialogue : Interactable
    {
        public string DialogueNode = "";

        protected override bool OnInteract(Player.Player player)
        {
            if (string.IsNullOrEmpty(DialogueNode) || DialogueNode.Trim().Length == 0) return false;
            player.StartDialogue(DialogueNode);
            return true;
        }
    }
}