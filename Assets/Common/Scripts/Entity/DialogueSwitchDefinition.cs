﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Common.Scripts.Entity
{
    [Serializable]
    [SuppressMessage("ReSharper", "UnassignedField.Global")]
    public struct DialogueSwitchDefinition
    {
        public Dialogue Dialogue;
        public string NewDialogueNode;
    }
}