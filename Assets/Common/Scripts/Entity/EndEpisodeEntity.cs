﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Common.Scripts.Entity
{
    public class EndEpisodeEntity : AbstractEntity
    {
        protected override void CustomStart()
        {
            enabled = false;
        }

        public override bool Interact(Player.Player player)
        {
            if (!enabled)
            {
                Debug.Log("Need to talk to Idrys one more time!");
                return false;
            }
            Debug.Log("Ending episode!!!");
            SceneManager.LoadScene("MainMenu");
            return true;
        }
    }
}