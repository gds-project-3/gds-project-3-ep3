﻿using UnityEngine;

namespace Common.Scripts.Entity
{
    public class Interactable : MonoBehaviour
    {
        public bool Interact(Player.Player player)
        {
            return OnInteract(player);
        }

        protected virtual bool OnInteract(Player.Player player)
        {
            return false;
        }
    }
}