﻿using Yarn.Unity;

namespace Common.Scripts.Entity
{
    public class Popup : Interactable
    {
        public DialogueRunner PopupDialogueRunner;
        public string PopupNode = "";

        protected override bool OnInteract(Player.Player player)
        {
            if (string.IsNullOrEmpty(PopupNode) || PopupNode.Trim().Length == 0) return false;
            ShowPopup(PopupNode);
            return true;
        }

        private void ShowPopup(string nodeName)
        {
            if (!PopupDialogueRunner.isDialogueRunning) PopupDialogueRunner.StartDialogue(nodeName);
        }
    }
}