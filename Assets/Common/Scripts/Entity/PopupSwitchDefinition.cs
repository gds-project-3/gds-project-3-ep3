﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Common.Scripts.Entity
{
    [Serializable]
    [SuppressMessage("ReSharper", "UnassignedField.Global")]
    public struct PopupSwitchDefinition
    {
        public Popup Popup;
        public string NewPopupNode;
    }
}