﻿using Anima2D;
using SpriteGlow;
using UnityEngine;

namespace Common.Scripts.Entity
{
    public abstract class AbstractEntity : MonoBehaviour
    {
        private Player.Player _playerRef;
        private SpriteGlowEffect _spriteGlowEffect;
        private SpriteRenderer _spriteRenderer;
        public float GlowBrightness = 2;
        public int GlowOutlineWidth = 10;

        private void Awake()
        {
            _spriteGlowEffect = GetComponent<SpriteGlowEffect>();
            if (_spriteGlowEffect == null) _spriteGlowEffect = gameObject.AddComponent<SpriteGlowEffect>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        protected virtual void CustomStart()
        {
        }

        private void Start()
        {
            _playerRef = FindObjectOfType<Player.Player>();
            LightOff();
            CustomStart();
        }

        private void OnMouseEnter()
        {
            if (_playerRef.InInteractionRange(transform)) LightOn();
        }

        private void OnMouseExit()
        {
            LightOff();
        }

        public void LightOn()
        {
            if (_spriteGlowEffect != null)
            {
                _spriteGlowEffect.OutlineWidth = GlowOutlineWidth;
                _spriteGlowEffect.GlowBrightness = GlowBrightness;
            }
        }

        private void LightOff()
        {
            _spriteGlowEffect.OutlineWidth = 0;
        }

        public virtual bool Interact(Player.Player player)
        {
            return true;
        }

        private void ApplySortingOrder()
        {
            if (_spriteRenderer != null)
            {
                _spriteRenderer.sortingOrder = (int) Camera.main.WorldToScreenPoint(_spriteRenderer.bounds.min).y * -1;
            }
        }

        private void LateUpdate()
        {
            ApplySortingOrder();
        }
    }
}