﻿namespace Common.Scripts.Entity
{
    public class SimpleEntity : AbstractEntity
    {
        public Dialogue Dialogue;
        public Item Item;
        public Popup Popup;

        public override bool Interact(Player.Player player)
        {
            return TryInteractWith(player, Dialogue) || //
                   TryInteractWith(player, Popup) || //
                   TryInteractWith(player, Item);
        }

        private static bool TryInteractWith(Player.Player player, Interactable interaction)
        {
            if (interaction == null || !interaction.isActiveAndEnabled) return false;
            return interaction.Interact(player);
        }
    }
}