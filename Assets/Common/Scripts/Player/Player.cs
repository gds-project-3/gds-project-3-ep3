﻿using System.Collections.Generic;
using Anima2D;
using Common.Scripts.Entity;
using UnityEngine;
using Yarn.Unity;

namespace Common.Scripts.Player
{
    public class Player : MonoBehaviour
    {
        public float MoveSpeed = 8f;
        public float InteractionRadius = 2f;
        public DialogueRunner PlayerDialogueRunner;
        public SkinnedMeshRenderer SortingBoundRenderer;

        private Rigidbody2D _rigidBody;
        private CapsuleCollider2D _capsuleCollider2D;
        private Animator _animator;
        private Vector2 _velocity;
        private Vector2 _move;
        private bool _interact;
        private Camera _camera;
        private readonly List<Item> _items = new List<Item>();
        private PlayerAudio _playerAudio;
        private bool _playerInputBlocked;
        private SpriteMeshInstance[] _spriteMeshInstances;
        private List<int> _originalOrderInLayer;

        private void Start()
        {
            _animator = GetComponent<Animator>();
            _rigidBody = GetComponent<Rigidbody2D>();
            _capsuleCollider2D = GetComponent<CapsuleCollider2D>();
            _camera = FindObjectOfType<Camera>();
            _playerAudio = GetComponent<PlayerAudio>();
            _spriteMeshInstances = GetComponentsInChildren<SpriteMeshInstance>();
            _originalOrderInLayer = new List<int>(_spriteMeshInstances.Length);
            foreach (var t in _spriteMeshInstances)
            {
                _originalOrderInLayer.Add(t.sortingOrder);
            }
        }

        private void Update()
        {
            ClearPlayerInput();
            if (!InteractionInProgress() && !_playerInputBlocked)
            {
                CapturePlayerInput();
            }

            ApplyGlow();
            TryInteract();
            ComputeVelocity();
            ComputeDirection();
            SetAnimation();
            PlayWalkSound();
        }

        private void PlayWalkSound()
        {
            if (_move.magnitude > 0.01)
            {
                _playerAudio.Walk();
            }
            else
            {
                _playerAudio.Stop();
            }
        }

        private void ApplyGlow()
        {
            var selected = Physics2D.Raycast(new Vector2(_camera.ScreenToWorldPoint(Input.mousePosition).x, _camera.ScreenToWorldPoint(Input.mousePosition).y),
                Vector2.zero, 0f);
            if (InInteractionRange(selected) && IsEntity(selected))
            {
                selected.transform.GetComponent<AbstractEntity>().LightOn();
            }
        }

        private bool InteractionInProgress()
        {
            return PlayerDialogueRunner != null && PlayerDialogueRunner.isDialogueRunning;
        }

        private void ClearPlayerInput()
        {
            _move = Vector2.zero;
            _interact = false;
        }

        private void TryInteract()
        {
            if (!_interact) return;
            var selected = Physics2D.Raycast(new Vector2(_camera.ScreenToWorldPoint(Input.mousePosition).x, _camera.ScreenToWorldPoint(Input.mousePosition).y),
                Vector2.zero, 0f);
            if (InInteractionRange(selected) && IsEntity(selected))
            {
                Debug.Log("Intercating with " + selected.transform.name);
                selected.transform.GetComponent<AbstractEntity>().Interact(this);
            }
        }

        private bool IsEntity(RaycastHit2D selected)
        {
            return selected.transform.GetComponent<AbstractEntity>() != null;
        }

        public bool InInteractionRange(Transform toCheck)
        {
            return (toCheck.position - GetPlayerPosition(_capsuleCollider2D)).magnitude <= InteractionRadius;
        }

        private bool InInteractionRange(RaycastHit2D selected)
        {
            if (!selected) return false;
            var entity = selected.transform.GetComponent<AbstractEntity>();
            if (entity != null)
            {
                return (entity.transform.position - GetPlayerPosition(_capsuleCollider2D)).magnitude <= InteractionRadius;
            }

            return false;
        }

        public void StartDialogue(string nodeName)
        {
            PlayerDialogueRunner.StartDialogue(nodeName);
        }

        private void SetAnimation()
        {
            _animator.SetFloat("walk", _move.magnitude);
        }

        private void ComputeDirection()
        {
            if (_move.x >= -0.01f && _move.x <= 0.01f) return;
            var tempVec = transform.localScale;
            tempVec.x = Mathf.Sign(_move.x);
            transform.localScale = tempVec;
        }

        private void ComputeVelocity()
        {
            _velocity = _move * MoveSpeed;
            _rigidBody.velocity = _velocity;
        }

        private void CapturePlayerInput()
        {
            _move.x = Input.GetAxis("Horizontal");
            _move.y = Input.GetAxis("Vertical");
            _interact = Input.GetButtonUp("Fire1");
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;
            // Flatten the sphere into a disk, which looks nicer in 2D games
            var coll = GetComponent<CapsuleCollider2D>();
            Gizmos.matrix = Matrix4x4.TRS(GetPlayerPosition(coll), Quaternion.identity, new Vector3(1, 1, 0));
            // Need to draw at position zero because we set position in the line above
            Gizmos.DrawWireSphere(Vector3.zero, InteractionRadius);
        }

        private Vector3 GetPlayerPosition(Collider2D coll)
        {
            return new Vector3(transform.position.x + coll.offset.x, transform.position.y + coll.offset.y);
        }

        public void AddItem(Item item)
        {
            _items.Add(item);
            _playerAudio.PlayPickItem();
        }

        public void ClearItems()
        {
            _items.Clear();
        }

        public bool ContainsItem(string itemName)
        {
            return _items.Find(i => i.ItemName.Equals(itemName));
        }

        public void Stop()
        {
            ClearPlayerInput();
            _velocity = Vector2.zero;
            _rigidBody.velocity = _velocity;
            _playerAudio.Stop();
        }

        public void BlockPlayerInput()
        {
            _playerInputBlocked = true;
        }

        public void AllowPlayerInput()
        {
            _playerInputBlocked = false;
        }

        private void ApplySortingOrder(SpriteMeshInstance spriteMeshInstance, int orderOffset)
        {
            spriteMeshInstance.sortingOrder = (int) Camera.main.WorldToScreenPoint(SortingBoundRenderer.bounds.min).y * -1 + orderOffset;
        }

        private void LateUpdate()
        {
            for (var i = 0; i < _spriteMeshInstances.Length; i++)
            {
                ApplySortingOrder(_spriteMeshInstances[i], _originalOrderInLayer[i]);
            }
        }
    }
}