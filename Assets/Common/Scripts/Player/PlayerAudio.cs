﻿using System.Collections.Generic;
using UnityEngine;

namespace Common.Scripts.Player
{
    public class PlayerAudio : MonoBehaviour
    {
        public List<AudioClip> WalkSounds = new List<AudioClip>();
        public List<AudioClip> CustomSounds = new List<AudioClip>();
        public AudioClip PickItemSound;
        private AudioClip _activeWalkSound;
        private AudioSource _walkAudioSource;
        private AudioSource _otherAudioSource;

        private void Start()
        {
            if (WalkSounds.Count > 0)
            {
                _activeWalkSound = WalkSounds[0];
            }

            _walkAudioSource = GetComponents<AudioSource>()[0];
            _walkAudioSource.clip = _activeWalkSound;
            _otherAudioSource = GetComponents<AudioSource>()[1];
        }

        public void Walk()
        {
            if (_activeWalkSound == null) return;
            if (_activeWalkSound == _walkAudioSource.clip && _walkAudioSource.isPlaying) return;
            _walkAudioSource.clip = _activeWalkSound;
            _walkAudioSource.loop = true;
            _walkAudioSource.Play();
        }

        public void Stop()
        {
            _walkAudioSource.Stop();
        }

        public void ChangeWalkSound(int index)
        {
            _activeWalkSound = WalkSounds[index];
        }

        public void PlayCustomSound(int index)
        {
            _otherAudioSource.PlayOneShot(CustomSounds[index]);
        }

        public void PlayPickItem()
        {
            _otherAudioSource.PlayOneShot(PickItemSound);
        }
    }
}