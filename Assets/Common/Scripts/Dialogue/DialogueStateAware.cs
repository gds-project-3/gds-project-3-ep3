﻿using UnityEngine;

namespace Common.Scripts.Dialogue
{
    public class DialogueStateAware : MonoBehaviour
    {
        public virtual void OnDialogueStart(bool shouldBlockControls)
        {

        }

        public virtual void OnDialogueEnd()
        {

        }
    }
}