﻿using System.Collections;
using System.Text;
using UnityEngine;
using Yarn;
using Yarn.Unity;

namespace Common.Scripts.Dialogue
{
    public class PopupDialogueUI : DialogueUIBehaviour
    {
        public GameObject DialogueContainer;
        public TextMesh TextLine;
        public float TextSpeed = 0.015f;
        public float PopupAutoCloseDelay = 3f;
        private SpriteRenderer _background;

        private void Awake()
        {
            // Start by hiding the container, line and option buttons
            if (DialogueContainer != null)
                DialogueContainer.SetActive(false);
            TextLine.GetComponent<Renderer>().sortingLayerName = "Popup";
            _background = TextLine.GetComponentInChildren<SpriteRenderer>();
            PrintText("");
        }

        public override IEnumerator RunLine(Line line)
        {
            if (TextSpeed > 0.0f)
            {
                // Display the line one character at a time
                var stringBuilder = new StringBuilder();

                foreach (var c in line.text)
                {
                    stringBuilder.Append(c);
                    PrintText(stringBuilder.ToString());
                    yield return new WaitForSeconds(TextSpeed);
                }
            }
            else
            {
                PrintText(line.text);
            }
        }

        private void PrintText(string text)
        {
            TextLine.text = text;
            if (_background != null)
            {
                _background.size = new Vector2(TextLine.GetComponent<Renderer>().bounds.size.x, TextLine.GetComponent<Renderer>().bounds.size.y) * 1.1f;
            }
        }

        public override IEnumerator RunOptions(Options optionsCollection, OptionChooser optionChooser)
        {
            yield break;
        }

        public override IEnumerator RunCommand(Yarn.Command command)
        {
            yield break;
        }

        /// <inheritdoc />
        public override IEnumerator DialogueStarted()
        {
            Debug.Log("Dialogue starting!");

            // Enable the dialogue controls.
            if (DialogueContainer != null)
                DialogueContainer.SetActive(true);

            yield break;
        }

        /// <inheritdoc />
        public override IEnumerator DialogueComplete()
        {
            Debug.Log("Complete!");

            yield return new WaitForSeconds(PopupAutoCloseDelay);
            // Hide the dialogue interface.
            PrintText("");
            if (DialogueContainer != null)
                DialogueContainer.SetActive(false);

        }
    }
}