﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Scripts.Dialogue.Command;
using UnityEngine;
using UnityEngine.UI;

namespace Common.Scripts.Dialogue
{
    public class DialogueUI : Yarn.Unity.DialogueUIBehaviour
    {
        /// The object that contains the dialogue and the options.
        /** This object will be enabled when conversation starts, and
         * disabled when it ends.
         */
        public GameObject DialogueContainer;

        /// The UI element that displays lines
        public Text LineText;

        /// A delegate (ie a function-stored-in-a-variable) that
        /// we call to tell the dialogue system about what option
        /// the user selected
        private Yarn.OptionChooser _setSelectedOption;

        /// How quickly to show the text, in seconds per character
        [Tooltip("How quickly to show the text, in seconds per character")]
        public float TextSpeed = 0.025f;

        /// The buttons that let the user choose an option
        public List<Button> OptionButtons;

        public Action DialogueStartedCallback;
        public Action DialogueCompletedCallback;

        private readonly List<BaseCommand> _commands = new List<BaseCommand>();

        private void Awake()
        {
            // Start by hiding the container, line and option buttons
            if (DialogueContainer != null)
                DialogueContainer.SetActive(false);

            LineText.gameObject.SetActive(false);

            foreach (var button in OptionButtons)
            {
                button.gameObject.SetActive(false);
            }
        }

        private void Start()
        {
            _commands.AddRange(FindObjectsOfType<BaseCommand>());
        }

        /// <inheritdoc />
        public override IEnumerator RunLine(Yarn.Line line)
        {
            // Show the text
            LineText.gameObject.SetActive(true);

            if (TextSpeed > 0.0f)
            {
                // Display the line one character at a time
                var stringBuilder = new StringBuilder();

                foreach (char c in line.text)
                {
                    stringBuilder.Append(c);
                    LineText.text = stringBuilder.ToString();
                    yield return new WaitForSeconds(TextSpeed);
                    if (!Input.anyKeyDown) continue;
                    LineText.text = line.text;
                    yield return new WaitForSeconds(TextSpeed * 3);
                    break;
                }
            }
            else
            {
                // Display the line immediately if textSpeed == 0
                LineText.text = line.text;
            }

            // Wait for any user input
            while (Input.anyKeyDown == false)
            {
                yield return null;
            }

            // Hide the text and prompt
            //LineText.gameObject.SetActive (false);
        }

        /// <inheritdoc />
        public override IEnumerator RunOptions(Yarn.Options optionsCollection,
            Yarn.OptionChooser optionChooser)
        {
            // Do a little bit of safety checking
            if (optionsCollection.options.Count > OptionButtons.Count)
            {
                Debug.LogWarning("There are more options to present than there are" +
                                 "buttons to present them in. This will cause problems.");
            }

            // Display each option in a button, and make it visible
            var i = 0;
            foreach (var optionString in optionsCollection.options)
            {
                OptionButtons[i].gameObject.SetActive(true);
                OptionButtons[i].GetComponentInChildren<Text>().text = optionString;
                i++;
            }

            // Record that we're using it
            _setSelectedOption = optionChooser;

            // Wait until the chooser has been used and then removed (see SetOption below)
            while (_setSelectedOption != null)
            {
                yield return null;
            }

            // Hide all the buttons
            foreach (var button in OptionButtons)
            {
                button.gameObject.SetActive(false);
            }
        }

        /// Called by buttons to make a selection.
        public void SetOption(int selectedOption)
        {
            // Call the delegate to tell the dialogue system that we've
            // selected an option.
            _setSelectedOption(selectedOption);

            // Now remove the delegate so that the loop in RunOptions will exit
            _setSelectedOption = null;
        }

        /// <inheritdoc />
        public override IEnumerator RunCommand(Yarn.Command command)
        {
            // "Perform" the command
            Debug.Log("Command: " + command.text);
            var splitted = command.text.Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);
            var commandName = splitted[0];
            _commands.FindAll(c => c.CommandName.Equals(commandName)).ForEach(c => c.Process(splitted));
            yield break;
        }

        /// <inheritdoc />
        public override IEnumerator DialogueStarted()
        {
            Debug.Log("Dialogue starting!");

            // Enable the dialogue controls.
            if (DialogueContainer != null)
                DialogueContainer.SetActive(true);

            if (DialogueStartedCallback != null)
            {
                DialogueStartedCallback();
            }

            yield break;
        }

        /// <inheritdoc />
        public override IEnumerator DialogueComplete()
        {
            Debug.Log("Complete!");

            // Hide the dialogue interface.
            if (DialogueContainer != null)
                DialogueContainer.SetActive(false);

            if (DialogueCompletedCallback != null)
            {
                DialogueCompletedCallback();
            }

            yield break;
        }
    }
}