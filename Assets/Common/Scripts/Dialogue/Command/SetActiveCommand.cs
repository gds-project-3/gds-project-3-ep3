﻿using UnityEngine;

namespace Common.Scripts.Dialogue.Command
{
    public class SetActiveCommand : BaseCommand
    {
        public GameObject ToChange;

        public override void Process(string[] commandParams)
        {
            base.Process(commandParams);
            ToChange.SetActive(bool.Parse(commandParams[1]));
        }
    }
}