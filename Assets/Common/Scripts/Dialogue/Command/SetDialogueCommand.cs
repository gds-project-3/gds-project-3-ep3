﻿using Common.Scripts.Entity;
using UnityEngine;

namespace Common.Scripts.Dialogue.Command
{
    public class SetDialogueCommand : BaseCommand
    {
        public DialogueSwitchDefinition Definition;

        public override void Process(string[] commandParams)
        {
            base.Process(commandParams);
            Definition.Dialogue.DialogueNode = Definition.NewDialogueNode;
        }
    }
}