﻿using UnityEngine;

namespace Common.Scripts.Dialogue.Command
{
    public class SetPositionCommand : BaseCommand
    {
        public Transform ToChange;
        public Transform TargetPosition;

        public override void Process(string[] commandParams)
        {
            base.Process(commandParams);
            ToChange.position = TargetPosition.position;
        }
    }
}