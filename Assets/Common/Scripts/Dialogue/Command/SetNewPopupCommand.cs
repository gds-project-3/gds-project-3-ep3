﻿using Common.Scripts.Entity;

namespace Common.Scripts.Dialogue.Command
{
    public class SetNewPopupCommand : BaseCommand
    {
        public PopupSwitchDefinition Definition;

        public override void Process(string[] commandParams)
        {
            base.Process(commandParams);
            Definition.Popup.PopupNode = Definition.NewPopupNode;
        }
    }
}