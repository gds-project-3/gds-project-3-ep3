﻿using UnityEngine;

namespace Common.Scripts.Dialogue.Command
{
    public class SetEnableCommand :  BaseCommand
    {
        public MonoBehaviour ToChange;

        public override void Process(string[] commandParams)
        {
            base.Process(commandParams);
            ToChange.enabled = true;
        }
    }
}