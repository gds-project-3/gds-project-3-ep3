﻿using UnityEngine;

namespace Common.Scripts.Dialogue.Command
{
    public class BaseCommand : MonoBehaviour
    {
        public string CommandName;

        public virtual void Process(string[] commandParams)
        {
            Debug.Log("Processing command: " + CommandName + " with params: " + string.Join(",", commandParams));
        }
    }
}