﻿using System;
using System.Collections;
using UnityEngine;

namespace Common.Scripts.Dialogue.Command
{
    public class FadeCommand : BaseCommand
    {
        public float FadeInSpeed = 3f;
        public float FadeInOutSpeed = 3f;
        public BaseCommand FadeInCommand;
        public BaseCommand FadeOutCommand;
        private Texture2D _blk;
        private float _alpha;

        public override void Process(string[] commandParams)
        {
            base.Process(commandParams);
            StartCoroutine(FadeInAndOut(
                () =>
                {
                    if (FadeInCommand != null) FadeInCommand.Process(commandParams);
                },
                () =>
                {
                    if (FadeOutCommand != null) FadeOutCommand.Process(commandParams);
                }
                ));
        }

        private IEnumerator FadeInAndOut(Action onFadeInCallback, Action onFadeOutCallback)
        {
            _alpha = 0;
            while (_alpha < 1)
            {
                _alpha += Time.deltaTime * FadeInSpeed;
                _blk.SetPixel(0, 0, new Color(0, 0, 0, _alpha));
                _blk.Apply();
                yield return null;
            }
            onFadeInCallback();
            while (_alpha > 0)
            {
                _blk.SetPixel(0, 0, new Color(0, 0, 0, _alpha));
                _blk.Apply();
                _alpha -= Time.deltaTime * FadeInOutSpeed;
                yield return null;
            }

            onFadeOutCallback();
        }

        private void Start()
        {
            //make a tiny black texture
            _blk = new Texture2D(1, 1);
            _blk.SetPixel(0, 0, new Color(0, 0, 0, 0));
            _blk.Apply();
        }

        private void OnGUI()
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), _blk);
        }
    }
}